# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import shutil
from pathlib import Path
from shutil import copy2

import pytest
from microlib import database
from pyfakefs.fake_filesystem_unittest import Pause

from zano.core.bundle import Bundle
from zano.core.fs_nodes import Node
from zano.core.env import ZANO_LOCAL_SHARE
from zano.core.errors import ConfigFileError, NoSuchBundleError

TESTDB_PATH = Path(__file__).parent / 'data/inodes.sqlite3'


def test_instanciation(fs):
    with pytest.raises(NoSuchBundleError) as excinfo:
        b = Bundle('CLG')
    assert str(excinfo.value) == f'Found no bundle named CLG configured in '\
                                 f'{ZANO_LOCAL_SHARE}.'

    # Normal content
    content = """[synced]
filters = []

[synced.side1]
name = 'HOME'
path = '/path/to/root1/'
backup = '/drive1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'
backup = '/drive2/'

[replicas.CLG1]
path = '/path/to/replica1/'
filters = []

[replicas.CLG2]
path = '/path/to/replica2/'

[replicas.CLG3]
path = '/path/to/replica3/'
filters = []"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    assert b.filters == []
    assert b.synced == {'side1': {'name': 'HOME',
                                  'path': Node('/path/to/root1/'),
                                  'backup': Node('/drive1/')},
                        'side2': {'name': 'NOMAD',
                                  'path': Node('/path/to/root2/'),
                                  'backup': Node('/drive2/')}}
    assert b.replicas == \
        {'CLG1': {'path': Node('/path/to/replica1/'), 'filters': []},
         'CLG2': {'path': Node('/path/to/replica2/'), 'filters': []},
         'CLG3': {'path': Node('/path/to/replica3/'), 'filters': []}}
    bundle_dir = ZANO_LOCAL_SHARE / 'CLG'
    assert b.paths == {'config': Node(bundle_dir / 'config.toml'),
                       'ts': Node(bundle_dir / 'ts.toml'),
                       'db': Node(bundle_dir / 'inodes.sqlite3'),
                       'dir': Node(bundle_dir)}

    # Normal, minimal content
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    assert b.filters == []
    assert b.synced == {'side1': {'name': 'HOME',
                                  'path': Node('/path/to/root1/'),
                                  'backup': None},
                        'side2': {'name': 'NOMAD',
                                  'path': Node('/path/to/root2/'),
                                  'backup': None}}
    assert not b.replicas
    bundle_dir = ZANO_LOCAL_SHARE / 'CLG'
    assert b.paths == {'config': Node(bundle_dir / 'config.toml'),
                       'ts': Node(bundle_dir / 'ts.toml'),
                       'db': Node(bundle_dir / 'inodes.sqlite3'),
                       'dir': Node(bundle_dir)}

    # Normal content without any filters defined.
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'

[replicas.CLG1]
path = '/path/to/replica1/'

[replicas.CLG2]
path = '/path/to/replica2/'

[replicas.CLG3]
path = '/path/to/replica3/'
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    assert b.filters == []
    assert b.synced == {'side1': {'name': 'HOME',
                                  'path': Node('/path/to/root1/'),
                                  'backup': None},
                        'side2': {'name': 'NOMAD',
                                  'path': Node('/path/to/root2/'),
                                  'backup': None}}
    assert b.replicas == \
        {'CLG1': {'path': Node('/path/to/replica1/'), 'filters': []},
         'CLG2': {'path': Node('/path/to/replica2/'), 'filters': []},
         'CLG3': {'path': Node('/path/to/replica3/'), 'filters': []}}

    # Normal content with all filters defined.
    content = """[synced]
filters = ['mask1/*', 'path/to/mask2/*']

[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'

[replicas.CLG1]
path = '/path/to/replica1/'
filters = ['maskA/']

[replicas.CLG2]
path = '/path/to/replica2/'
filters = ['maskB/']

[replicas.CLG3]
path = '/path/to/replica3/'
filters = ['maskC/']
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    assert b.filters == ['mask1/*', 'path/to/mask2/*']
    assert b.synced == {'side1': {'name': 'HOME',
                                  'path': Node('/path/to/root1/'),
                                  'backup': None},
                        'side2': {'name': 'NOMAD',
                                  'path': Node('/path/to/root2/'),
                                  'backup': None}}
    assert b.replicas == \
        {'CLG1': {'path': Node('/path/to/replica1/'), 'filters': ['maskA/']},
         'CLG2': {'path': Node('/path/to/replica2/'), 'filters': ['maskB/']},
         'CLG3': {'path': Node('/path/to/replica3/'), 'filters': ['maskC/']}}


def test_instanciation_errors(fs):
    testpath = ZANO_LOCAL_SHARE / 'CLG/config.toml'
    # Missing [synced] table
    content = """[sync]
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'Missing [synced] table in config file located at: {testpath}'

    # No [replicas] is OK, but [synced] must contain side1 and side2
    content = """[synced]
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'The [synced] table must define exactly two sides in config file '\
        f'located at: {testpath}'

    # [synced] does not define 2 sides
    content = """[synced.side1]
name = 'HOME'
path = '/home/'

[replicas]
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'The [synced] table must define exactly two sides in config file '\
        f'located at: {testpath}'

    content = """[synced.side1]
name = 'HOME'
path = '/home/'

[synced.side2]
name = 'NOMAD'
path = '/nomad/'

[synced.side3]
name = 'EXTRA'
path = '/extra/'

[replicas]
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'The [synced] table must define exactly two sides in config file '\
        f'located at: {testpath}'

    # [synced] contains a side3 instead of a side2
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side3]
name = 'NOMAD'
path = '/path/to/root2/'

[replicas.CLG1]
path = '/path/to/replica1/'
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'Missing "side2" in [synced] table in config file located at: '\
        f'{testpath}'

    # Missing name in a [synced] side
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
path = '/path/to/root2/'

[replicas.CLG1]
path = '/path/to/replica1/'
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'Missing "name" in [synced.side2] table in config file located at: '\
        f'{testpath}'

    # Missing path in a replica
    content = """[synced.side1]
name = 'HOME'
path = '/home/'

[synced.side2]
name = 'NOMAD'
path = '/nomad/'

[replicas.CLG1]
filters = ['mask1/']
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'Missing \'path\' in replica "CLG1" in config file located at: '\
        f'{testpath}'

    # Extraneous entry in a synced
    content = """[synced.side1]
name = 'HOME'
path = '/home/'
date = 'now'

[synced.side2]
name = 'NOMAD'
path = '/nomad/'
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'Unknown "date" in [synced.side1] table in config file located '\
        f'at: {testpath}'

    # Filters defined outside [synced] or [replicas]
    content = """filters = ['mask1/*', 'mask2/*']

[synced.side1]
name = 'HOME'
path = '/home/'

[synced.side2]
name = 'NOMAD'
path = '/nomad/'
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    with pytest.raises(ConfigFileError) as excinfo:
        Bundle('CLG')
    assert str(excinfo.value) == \
        f'Extraneous entries: filters, in config file located at: '\
        f'{testpath}'


def test_idb_creation(fs, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    testdb_path = ZANO_LOCAL_SHARE / 'CLG/inodes.sqlite3'
    b = Bundle('CLG')
    fs.create_file(testdb_path)
    assert b.paths['dir'].exists()
    with pytest.raises(FileExistsError) as excinfo:
        b.create_idb()
    assert str(excinfo.value) == '[Errno 17] File exists: '\
        f'\'{testdb_path}\''
    empty_db = Node(__file__).parent / 'data/empty.sqlite3'
    mocked_CM = database.ContextManager(empty_db)
    mocker.patch('microlib.database.ContextManager', return_value=mocked_CM)
    fs.remove_object(b.paths['dir'].path)
    assert not b.paths['dir'].exists()
    b.create_idb()
    assert b.paths['db'].exists()
    with database.ContextManager(b.paths['db']) as cursor:
        idb = database.Operator(cursor)
        assert idb.table_exists('files')
        assert idb.table_exists('dirs')
        assert idb.get_table('files', include_headers=True) \
            == [('id', 'inode1', 'inode2')]
        assert idb.get_table('dirs', include_headers=True) \
            == [('id', 'inode1', 'inode2')]
    with Pause(fs):
        empty_db = Path(__file__).parent / 'data/empty.sqlite3'
        empty_db.unlink()
        empty_db_copy = Path(__file__).parent / 'data/empty_copy.sqlite3'
        copy2(empty_db_copy, empty_db)


def test_idb(fs):
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    cm = database.ContextManager
    with cm(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        assert b.is_known_inode('dirs', 'inode1', 1316924)
        assert not b.is_known_inode('files', 'inode2', 1316924)
        b.insert_inodes('dirs', [(5909851, 5916476), (4469577, 7091739)])
        assert b.get_inode_matching('dirs', 'inode1', 1316924) == 1317969
        assert b.get_inode_matching('files', 'inode2', 1067963) == 1067277
        assert b.get_inode_matching('dirs', 'inode1', 1316925) is None
        b.remove_inodes('files', 'inode1', [1317970])
        b.remove_inodes('dirs', 'inode1', [1316924])
        assert b.get_inode_matching('files', 'inode1', 1317970) is None
        assert b.get_inode_matching('dirs', 'inode1', 1316924) is None
        b.remove_inodes('dirs', 'inode1', [1316924])


def test_rebuild_idb(fs, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 10
    dir2_1 = fs.create_dir('/side1/dir2')
    dir2_1.st_ino = 100
    dir3_1 = fs.create_dir('/side1/dir2/dir3')
    dir3_1.st_ino = 1000
    f1_1 = fs.create_file('/side1/f1.txt')
    f1_1.st_ino = 2
    f2_1 = fs.create_file('/side1/dir2/dir3/f2.txt')
    f2_1.st_ino = 3

    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 20
    dir2_2 = fs.create_dir('/side2/dir2')
    dir2_2.st_ino = 200
    dir3_2 = fs.create_dir('/side2/dir2/dir3')
    dir3_2.st_ino = 2000
    f1_2 = fs.create_file('/side2/f1.txt')
    f1_2.st_ino = 8
    f2_2 = fs.create_file('/side2/dir2/dir3/f2.txt')
    f2_2.st_ino = 27

    mocked_CM = database.ContextManager(TESTDB_PATH)
    mocker.patch('microlib.database.ContextManager', return_value=mocked_CM)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH) as cursor:
        b.set_idb(cursor)
        b.rebuild_idb()
        assert b.idb.list_tables() == ['dirs', 'files']
        assert b.idb.get_table('dirs', include_headers=True) == \
            [('id', 'inode1', 'inode2'),
             ('1', '10', '20'),
             ('2', '100', '200'),
             ('3', '1000', '2000')]
        assert b.idb.get_table('files', include_headers=True) == \
            [('id', 'inode1', 'inode2'),
             ('1', '3', '27'),
             ('2', '2', '8')]

    with Pause(fs):
        TESTDB_PATH.unlink()
        testdb_copy = Path(__file__).parent / 'data/inodes_original.sqlite3'
        shutil.copy2(testdb_copy, TESTDB_PATH)


def test_get_timestamp(fs):
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    content = """synced = 1630068206.1940188
CLG1 = 1630068206.1940188
CLG2 = 1625219684.4386852"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/ts.toml', contents=content)
    assert b.get_timestamp() == 1630068206.1940188
    assert b.get_timestamp('CLG2') == 1625219684.4386852


def test_set_timestamp(fs, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    content = """synced = 1630068206.1940188
CLG1 = 1630068206.1940188
CLG2 = 1625219684.4386852"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/ts.toml', contents=content)
    m = mocker.patch('zano.core.bundle.time')
    m.side_effect = [1630068224.1541214]
    m2 = mocker.patch('toml.dump')
    b.set_timestamp()
    assert m2.call_args[0][0] == {'synced': 1630068224.1541214,
                                  'CLG1': 1630068206.1940188,
                                  'CLG2': 1625219684.4386852}
    fs.remove_object(ZANO_LOCAL_SHARE / 'CLG/ts.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/ts.toml', contents=content)
    m = mocker.patch('zano.core.bundle.time')
    m.side_effect = [1630068209.01020304]
    b.set_timestamp('CLG1')
    assert m2.call_args[0][0] == {'synced': 1630068206.1940188,
                                  'CLG1': 1630068209.01020304,
                                  'CLG2': 1625219684.4386852}


def test_connected_replicas(fs, capsys):
    root1 = Node(__file__).parent / 'dirs/home'
    root2 = Node(__file__).parent / 'dirs/nomad'
    content = f"""[synced.side1]
name = 'HOME'
path = '{root1}'

[synced.side2]
name = 'NOMAD'
path = '{root2}'

[replicas.CLG1]
path = '/path/to/replica1/'
filters = []

[replicas.CLG2]
path = '/path/to/replica2/'
filters = []

[replicas.CLG3]
path = '/path/to/replica3/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    # fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_dir('/path/to/replica1/')
    fs.create_dir('/path/to/replica2/')
    b = Bundle('BUNDLE')
    assert b.connected_replicas() \
        == {'CLG1': {'path': Node('/path/to/replica1/'), 'filters': []},
            'CLG2': {'path': Node('/path/to/replica2/'), 'filters': []}}
    captured = capsys.readouterr()
    assert captured.out == 'Warning: Cannot find replica CLG3 '\
        '(path: /path/to/replica3)\n'


def test_fetch_new_dirs_and_files(fs, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)
    b = Bundle('CLG')
    mocked_cdi = mocker.patch('zano.core.bundle.collect_dirs_inodes')
    mocked_cdi.side_effect = \
        [[(1316923, 'dir1/'), (1316924, 'dir2'), (1317987, 'dir3'),
          (1316331, 'dir4/'), (1316339, 'new/')],
         [(1317712, 'dir1/'), (1317969, 'dir2'), (1317994, 'dir3'),
          (1315093, 'dir4/'), (1318597, 'new/'), (1315967, 'other/')]
         ]
    mocked_cfi = mocker.patch('zano.core.bundle.collect_files_inodes')
    mocked_cfi.side_effect = \
        [[(1067612, 'file1'), (1067502, 'file2'), (1067277, 'file3'),
          (1318315, 'file4'), (1317970, 'file5'), (1318030, 'file6'),
          (1060419, 'newfile')],
         [(1065664, 'file1'), (1067962, 'file2'), (1067963, 'file3'),
          (1316420, 'file4'), (1317976, 'file5'), (1318067, 'file6'),
          (1066819, 'newfile'), (1066175, 'otherfile')]
         ]
    cm = database.ContextManager
    with cm(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        # BEWARE: the mocks do not return Nodes, as should collect_*_inodes()
        assert b.fetch_new_dirs() == {'side1': [(1316339, 'new/')],
                                      'side2': [(1318597, 'new/'),
                                                (1315967, 'other/')]}
        assert b.fetch_new_files() == {'side1': [(1060419, 'newfile')],
                                       'side2': [(1066819, 'newfile'),
                                                 (1066175, 'otherfile')]}


def test_fetch_new_dirs_filters(fs):
    content = """[synced]
filters = ['dir1/*']

[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'CLG/config.toml', contents=content)

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712

    d2_1 = fs.create_dir('/side1/dir2/')
    d2_1.st_ino = 1316924
    d2_2 = fs.create_dir('/side2/dir2/')
    d2_2.st_ino = 1317969

    d3_1 = fs.create_dir('/side1/dir1/dir3/')
    d3_1.st_ino = 1317987
    d3_2 = fs.create_dir('/side2/dir1/dir3/')
    d3_2.st_ino = 1317994

    n1 = fs.create_dir('/side1/dir1/dir3/new1')
    n1.st_ino = 1310001
    n2 = fs.create_dir('/side2/dir1/dir3/new2')
    n2.st_ino = 1310002

    d4_1 = fs.create_dir('/side1/dir2/dir4')
    d4_1.st_ino = 1316331
    d4_2 = fs.create_dir('/side2/dir2/dir4')
    d4_2.st_ino = 1315093

    n3 = fs.create_dir('/side1/dir2/dir4/new3')
    n3.st_ino = 1310003
    n4 = fs.create_dir('/side2/dir2/dir4/new4')
    n4.st_ino = 1310004

    b = Bundle('CLG')

    cm = database.ContextManager
    with cm(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        assert b.fetch_new_dirs() == {'side1': [(1310003,
                                                 Node('dir2/dir4/new3'))],
                                      'side2': [(1310004,
                                                 Node('dir2/dir4/new4'))]}
