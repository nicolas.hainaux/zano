# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import shutil
from pathlib import Path

import pytest
from microlib import database
from pyfakefs.fake_filesystem_unittest import Pause

from zano.core.fs_nodes import Node
from zano.core.bundle import Bundle
from zano.core.env import ZANO_LOCAL_SHARE
from zano.core.commands import pair, sync
from zano.core.errors import CommandError, MissingPathError

TESTDB_PATH = Path(__file__).parent / 'data/inodes.sqlite3'


def test_pair_errors(fs):
    content = """[synced.side1]
name = 'HOME'
path = '/path/to/root1/'

[synced.side2]
name = 'NOMAD'
path = '/path/to/root2/'

[replicas]
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fakedb = Node(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(fakedb.path)

    with pytest.raises(MissingPathError) as excinfo:
        pair('BUNDLE')
    assert str(excinfo.value) == \
        'HOME cannot be found (missing path: /path/to/root1)\n'\
        'NOMAD cannot be found (missing path: /path/to/root2)'

    fs.create_dir('/path/to/root1/')
    fs.create_dir('/path/to/root2/')
    with pytest.raises(CommandError) as excinfo:
        pair('BUNDLE')
    assert str(excinfo.value) == f'Bundle BUNDLE is already paired. Please '\
        f'either remove (or backup) the database yourself or use the sync '\
        f'command instead. Database is located at {fakedb.path}'

    root1 = Node(__file__).parent / 'dirs/root1'
    root2 = Node(__file__).parent / 'dirs/root2'
    fs.create_dir(root1.path)
    fs.create_dir(root2.path)
    fs.create_file(str(root1 / 'new'))
    fs.create_file(str(root1 / 'modified'))
    fs.create_file(str(root2 / 'modified'))
    fs.create_file(str(root1 / 'timestamp'))
    fs.create_file(str(root2 / 'timestamp'))
    fs.create_file(str(root1 / 'untouched'))
    fs.create_file(str(root2 / 'untouched'))
    content = f"""[synced.side1]
name = 'HOME'
path = '{root1}'

[synced.side2]
name = 'NOMAD'
path = '{root2}'

[replicas]
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'FAKE/config.toml', contents=content)
    fakedb = Node(ZANO_LOCAL_SHARE / 'FAKE/inodes.sqlite3')
    # fs.create_file(fakedb.path)

    with pytest.raises(CommandError) as excinfo:
        pair('FAKE')
    assert str(excinfo.value) == 'The pair command can be used to build the '\
        'inodes database between two tree structures that are already '\
        'synced. Please ensure the files are the same on both sides before '\
        'pairing.'


def test_pair(fs, mocker):
    fs.create_dir('/side1')
    fs.create_dir('/side2')
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas.CLG1]
path = '/drive1/'
"""
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 2917
    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 7192
    dir2_1 = fs.create_dir('/side1/dir1/dir2')
    dir2_1.st_ino = 8000
    dir2_2 = fs.create_dir('/side2/dir1/dir2')
    dir2_2.st_ino = 8001
    dir3_1 = fs.create_dir('/side1/dir3')
    dir3_1.st_ino = 500
    dir3_2 = fs.create_dir('/side2/dir3')
    dir3_2.st_ino = 506

    file1_1 = fs.create_file('/side1/file1')
    file1_1.st_ino = 20
    file1_2 = fs.create_file('/side2/file1')
    file1_2.st_ino = 21
    file2_1 = fs.create_file('/side1/file2')
    file2_1.st_ino = 56
    file2_2 = fs.create_file('/side2/file2')
    file2_2.st_ino = 57
    file3_1 = fs.create_file('/side1/dir3/file3')
    file3_1.st_ino = 1200
    file3_2 = fs.create_file('/side2/dir3/file3')
    file3_2.st_ino = 1300

    fs.create_file(ZANO_LOCAL_SHARE / 'FAKE/config.toml', contents=content)
    blank_db_path = Path(__file__).parent / 'data/blank.sqlite3'
    mocker.patch('zano.core.bundle.Bundle.create_idb')
    m = mocker.patch('zano.core.bundle.time')
    m.side_effect = [1630000000, 1630000010]
    mocked_CM = database.ContextManager(blank_db_path)
    mocker.patch('microlib.database.ContextManager', return_value=mocked_CM)

    pair('FAKE')

    with database.ContextManager(blank_db_path) as cursor:
        idb = database.Operator(cursor)
        assert idb.table_exists('files')
        assert idb.table_exists('dirs')
        assert idb.get_table('files', include_headers=True) \
            == [('id', 'inode1', 'inode2'),
                ('1', '1200', '1300'), ('2', '20', '21'), ('3', '56', '57'), ]
        assert idb.get_table('dirs', include_headers=True) \
            == [('id', 'inode1', 'inode2'),
                ('1', '2917', '7192'), ('2', '8000', '8001'),
                ('3', '500', '506')]
        assert Bundle('FAKE').get_timestamp() == 1630000000
        assert Bundle('FAKE').get_timestamp('CLG1') == 1630000010

    with Pause(fs):
        blank_db = Path(__file__).parent / 'data/blank.sqlite3'
        blank_db.unlink()
        blank_db_copy = Path(__file__).parent / 'data/blank_copy.sqlite3'
        shutil.copy2(blank_db_copy, blank_db)


def test_sync_error_no_idb(fs):
    # no idb (not paired)
    content = """[synced.side1]
name = 'HOME'
path = '/nothing/'

[synced.side2]
name = 'NOMAD'
path = '/unplugged/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fakedb_path = ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3'
    assert not Path(fakedb_path).is_file()
    with pytest.raises(CommandError) as excinfo:
        sync('BUNDLE')
    assert str(excinfo.value) == 'Bundle BUNDLE is not paired yet. '\
        'Please use the pair command before syncing.'


def test_sync_error_synced_side_missing(fs):
    # synced side missing
    content = """[synced.side1]
name = 'HOME'
path = '/nothing/'

[synced.side2]
name = 'NOMAD'
path = '/unplugged/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fakedb_path = ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3'
    fs.create_file(fakedb_path)
    with pytest.raises(MissingPathError) as excinfo:
        sync('BUNDLE')
    assert str(excinfo.value) == 'HOME cannot be found (missing path: '\
        '/nothing)\nNOMAD cannot be found (missing path: /unplugged)'


def test_sync_error_backup_before_path_troubles(fs):
    # --backup-before option enabled, but one backup path is missing
    content = """[synced.side1]
name = 'HOME'
path = '/myhome/'
backup = '/drive1'

[synced.side2]
name = 'NOMAD'
path = '/plugged/'
"""
    fake_cfg = fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml',
                              contents=content)
    fakedb_path = ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3'
    fs.create_file(fakedb_path)
    fs.create_dir('/myhome/')
    fs.create_dir('/drive1')
    fs.create_dir('/plugged')
    with pytest.raises(MissingPathError) as excinfo:
        sync('BUNDLE', backup_before=True)
    assert str(excinfo.value) == '--backup-before option requires '\
        '[synced.side2] to define a backup path (backup = /path/to/backup)'

    # --backup-before option enabled, but one backup path cannot be found
    fs.remove_object(fake_cfg.path)
    content = """[synced.side1]
name = 'HOME'
path = '/myhome/'
backup = '/drive1'

[synced.side2]
name = 'NOMAD'
path = '/plugged/'
backup = '/drive2'
"""
    fake_cfg = fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml',
                              contents=content)
    with pytest.raises(MissingPathError) as excinfo:
        sync('BUNDLE', backup_before=True)
    assert str(excinfo.value) == \
        'Cannot find backup directory for NOMAD: /drive2'


def test_sync_error_backup_after_path_troubles(fs, mocker):
    # --backup-after option enabled, but one backup path is missing
    content = """[synced.side1]
name = 'HOME'
path = '/myhome/'
backup = '/drive1'

[synced.side2]
name = 'NOMAD'
path = '/plugged/'
"""
    fake_cfg = fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml',
                              contents=content)
    fakedb_path = ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3'
    fs.create_file(fakedb_path)
    mocked_CM = database.ContextManager(TESTDB_PATH, testing=True)
    mocker.patch('zano.core.commands.database.ContextManager',
                 return_value=mocked_CM)
    mock_input = mocker.patch('builtins.input')
    mock_input.side_effect = ['Y', 'Y']
    fs.create_dir('/myhome/')
    fs.create_dir('/drive1')
    fs.create_dir('/plugged')
    with pytest.raises(MissingPathError) as excinfo:
        sync('BUNDLE', backup_after=True)
    assert str(excinfo.value) == '--backup-after option requires '\
        '[synced.side2] to define a backup path (backup = /path/to/backup)'

    # --backup-after option enabled, but one backup path cannot be found
    fs.remove_object(fake_cfg.path)
    content = """[synced.side1]
name = 'HOME'
path = '/myhome/'
backup = '/drive1'

[synced.side2]
name = 'NOMAD'
path = '/plugged/'
backup = '/drive2'
"""
    fake_cfg = fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml',
                              contents=content)
    with pytest.raises(MissingPathError) as excinfo:
        sync('BUNDLE', backup_after=True)
    assert str(excinfo.value) == \
        'Cannot find backup directory for NOMAD: /drive2'


def test_sync_backups(fs, mocker, capsys):
    content = """[synced.side1]
name = 'HOME'
path = '/side1'
backup = '/drive1'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
backup = '/drive2'
"""
    ts = """synced = 1640000500
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fakedb_path = ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3'
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml', contents=ts)
    fs.create_file(fakedb_path)

    fs.create_dir('/side1')
    fs.create_dir('/drive1')
    fs.create_dir('/side2')
    fs.create_dir('/drive2')

    mock_input = mocker.patch('builtins.input')
    mock_input.side_effect = ['Y']

    mocked_CM = database.ContextManager(TESTDB_PATH, testing=True)
    mocker.patch('zano.core.commands.database.ContextManager',
                 return_value=mocked_CM)

    sync('BUNDLE', backup_before=True, backup_after=True)

    captured = capsys.readouterr()
    assert captured.out == 'BACKING UP EACH SYNCED SIDE\n'\
        'Done\n'\
        'SYNCING DIRECTORIES\n'\
        'No change\n'\
        'SYNCING FILES\n'\
        'No change\n'\
        'BACKING UP EACH SYNCED SIDE\n'\
        'Done\n'


def test_sync(fs, mocker, capsys):
    config = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    ts = """synced = 1640000500
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=config)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml', contents=ts)

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d1_1.st_ctime = 1640000500
    d2_1 = fs.create_dir('/side1/DIR2/')
    d2_1.st_ino = 1316924
    d2_1.st_ctime = 1640000700
    d3_1 = fs.create_dir('/side1/DIR2/dir3/')
    d3_1.st_ino = 1317987
    d3_1.st_ctime = 1640000800
    d4_1 = fs.create_dir('/side1/DIR2/dir3/dir4')
    d4_1.st_ino = 1316331
    d4_1.st_ctime = 1640000700
    d9_1 = fs.create_dir('/side1/dir1/DIR9')
    d9_1.st_ino = 1320009
    d9_1.st_ctime = 1640000800
    d6_1 = fs.create_dir('/side1/dir1/DIR9/dir6')
    d6_1.st_ino = 1320003
    d6_1.st_ctime = 1640000800
    d8_1 = fs.create_dir('/side1/dir8')
    d8_1.st_ino = 1320007
    d8_1.st_ctime = 1640000400
    d7_1 = fs.create_dir('/side1/dir8/dir7')
    d7_1.st_ino = 1320005
    d7_1.st_ctime = 1640000900

    f1_1 = fs.create_file('/side1/file1')  # new on side1
    f1_1.st_ino = 1310000
    f2_1 = fs.create_file('/side1/dir8/dir7/file2')
    f2_1.st_ino = 1067612
    f2_1.st_ctime = 1640000600  # moved on side1, updated on side2
    f2_1.st_mtime = 1640000400  # moved on side1, updated on side2
    f3_1 = fs.create_file('/side1/dir1/DIR9/file3')  # deleted on side2
    f3_1.st_ino = 1067502
    f6_1 = fs.create_file('/side1/DIR2/file6')
    f6_1.st_ino = 1318315
    f6_1.st_ctime = 1640000400  # moved on side2, updated on side1
    f6_1.st_mtime = 1640000600  # moved on side2, updated on side1

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d1_2.st_ctime = 1640000600
    d2_2 = fs.create_dir('/side2/dir1/dir2/')
    d2_2.st_ino = 1317969
    d2_2.st_ctime = 1640000400
    d3_2 = fs.create_dir('/side2/DIR3/')
    d3_2.st_ino = 1317994
    d3_2.st_ctime = 1640000900
    d4_2 = fs.create_dir('/side2/DIR3/dir4')
    d4_2.st_ino = 1315093
    d4_2.st_ctime = 1640000700
    d5_2 = fs.create_dir('/side2/dir5')  # deleted on side1
    d5_2.st_ino = 1320002
    d5_2.st_ctime = 1640000900
    d6_2 = fs.create_dir('/side2/dir5/dir6')
    d6_2.st_ino = 1320004
    d6_2.st_ctime = 1640000400
    d7_2 = fs.create_dir('/side2/dir5/dir6/dir7')
    d7_2.st_ino = 1320006
    d7_2.st_ctime = 1640000800
    d8_2 = fs.create_dir('/side2/dir1/dir2/dir8')
    d8_2.st_ino = 1320008
    d8_2.st_ctime = 1640000800
    d9_2 = fs.create_dir('/side2/dir1/dir2/dir8/dir9')
    d9_2.st_ino = 1320010
    d9_2.st_ctime = 1640000300
    d10_2 = fs.create_dir('/side2/dir1/dir2/dir8/dir9/dir10')  # new on side2
    d10_2.st_ino = 1310002
    d10_2.st_ctime = 1640000950

    f2_2 = fs.create_file('/side2/DIR3/dir4/file2')
    f2_2.st_ino = 1065664
    f2_2.st_ctime = 1640000400  # moved on side1, updated on side2
    f2_2.st_mtime = 1640000600  # moved on side1, updated on side2
    f4_2 = fs.create_file('/side2/dir5/file4')  # new on side2
    f4_2.st_ino = 1310012
    f5_2 = fs.create_file('/side2/dir5/file5')  # deleted on side1
    f5_2.st_ino = 1067963
    f6_2 = fs.create_file('/side2/dir1/dir2/dir8/dir9/dir10/file6')
    f6_2.st_ino = 1316420
    f6_2.st_ctime = 1640000600  # moved on side2, updated on side1
    f6_2.st_mtime = 1640000400  # moved on side2, updated on side1
    f7_2 = fs.create_file('/side2/dir1/file7')  # deleted on side1
    f7_2.st_ino = 1317976
    f8_2 = fs.create_file('/side2/dir1/dir2/file8')  # new on side2
    f8_2.st_ino = 1310024

    def fake_send2trash(arg):
        fs.remove_object(arg)

    mocker.patch('zano.core.tasks.send2trash', side_effect=fake_send2trash)

    mocked_CM = database.ContextManager(TESTDB_PATH, testing=True)
    mocker.patch('zano.core.commands.database.ContextManager',
                 return_value=mocked_CM)

    mocker.patch('zano.core.fs_tools.subprocess.run')

    sync('BUNDLE')

    assert Node('/side1/dir1/DIR9/dir6').is_dir()
    assert Node('/side1/dir1/DIR9/dir10').is_dir()
    assert Node('/side1/DIR2/dir8/dir7').is_dir()
    assert Node('/side1/DIR3/dir4').is_dir()
    assert not Node('/side1/dir5').is_dir()

    assert Node('/side2/dir1/DIR9/dir6').is_dir()
    assert Node('/side2/dir1/DIR9/dir10').is_dir()
    assert Node('/side2/DIR2/dir8/dir7').is_dir()
    assert Node('/side2/DIR3/dir4').is_dir()
    assert not Node('/side2/dir5').is_dir()

    captured = capsys.readouterr()
    assert captured.out == 'SYNCING DIRECTORIES\n'\
        'NOMAD: MOVE dir1/dir2 TO DIR2\n'\
        'NOMAD: MOVE DIR2/dir8/dir9 TO dir1/DIR9\n'\
        'HOME: MOVE dir8 TO DIR2/dir8\n'\
        'HOME: MOVE DIR2/dir3 TO DIR3\n'\
        'HOME: NEW dir1/DIR9/dir10\n'\
        'NOMAD: MOVE dir5/dir6 TO dir1/DIR9/dir6\n'\
        'NOMAD: CAUTION: found NEW file dir5/file4 in to-be-deleted '\
        'directory dir5; you might want to get it back from the trashcan...\n'\
        'NOMAD: DELETE dir5\n'\
        'NOMAD: MOVE dir1/DIR9/dir6/dir7 TO DIR2/dir8/dir7\n'\
        'SYNCING FILES\n'\
        'NOMAD: MOVE DIR3/dir4/file2 TO DIR2/dir8/dir7/file2\n'\
        'HOME: MOVE DIR2/file6 TO dir1/DIR9/dir10/file6\n'\
        'HOME: DELETE dir1/DIR9/file3\n'\
        'NOMAD: NEW file1\n'\
        'HOME: NEW DIR2/file8\n'\
        'NOMAD: DELETE dir1/file7\n'\
        'HOME: UPDATE DIR2/dir8/dir7/file2\n'\
        'NOMAD: UPDATE dir1/DIR9/dir10/file6\n'
