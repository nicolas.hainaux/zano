# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

from pathlib import Path

import pytest

from zano.core.fs_nodes import Node
from zano.core.fs_tools import list_tree_structure
from zano.core.fs_tools import collect_newer_files
from zano.core.fs_tools import collect_missing_files
from zano.core.fs_tools import skim_missing_dirs
from zano.core.fs_tools import collect_paired_files_inodes
from zano.core.fs_tools import collect_paired_dirs_inodes
from zano.core.fs_tools import collect_dirs_inodes
from zano.core.fs_tools import collect_files_inodes
from zano.core.fs_tools import find_dir_matching_inode
from zano.core.fs_tools import find_file_matching_inode
from zano.core.fs_tools import probably_moved, probably_renamed
from zano.core.fs_tools import renamed_node, moved_node
from zano.core.fs_tools import probably_moved_and_renamed
from zano.core.fs_tools import _check_consistency
from zano.core.fs_tools import is_accepted
from zano.core.fs_tools import tree_structures_are_synced
from zano.core.fs_tools import rsync


def test_rsync_calls():
    side1 = Path(__file__).parent / 'data/dirs/side1'
    side2 = Path(__file__).parent / 'data/dirs/side2'

    f1_2 = side2 / 'file1'
    assert not f1_2.is_file()
    rsync(side1, side2, '-avz')
    assert f1_2.is_file()
    f1_2.unlink()

    side3 = Path(__file__).parent / 'data/dirs/side3'
    f1_3 = side3 / 'file1'
    f3 = side3 / 'subdir1/file3'
    assert not f3.exists()
    rsync(side1, side3, '-avz', filters=['subdir1/'])
    assert f1_3.is_file()
    assert not f3.exists()
    f1_3.unlink()


def test_check_consistency(fs):
    path1 = Node(__file__).parent / 'dirs/home/dirA/moved.txt'
    path2 = Node(__file__).parent / 'dirs/nomad/dirB/moved.txt'
    fs.create_file(str(path1))
    fs.create_file(str(path2))
    assert _check_consistency(path1, path2)
    assert _check_consistency(path1.parent, path2.parent)
    with pytest.raises(TypeError) as excinfo:
        _check_consistency(path1, path2.parent)
    assert str(excinfo.value) == 'Arguments should both be a file or both a '\
        'directory; got a file and a directory instead.'


def test_is_accepted(fs, capsys, mocker):
    fs.create_dir('/home/USER/dev/project/.git')
    fs.create_dir('/home/USER/dev2/project')
    # .git is in the default filters
    assert not is_accepted(Node('/home/USER/dev/project/.git'),
                           filters=[])
    # directories containing a '.git' subdirectory are not accepted
    assert not is_accepted(Node('/home/USER/dev/project'),
                           filters=[])
    assert not is_accepted(Node('/home/USER/dev2/project'),
                           filters=['/home/USER/dev2/*'])
    assert is_accepted(Node('/home/USER/dev2/project'), filters=[])

    fs.create_file('/side1/myapp.py')
    assert not is_accepted(Node('/side1/myapp.py'), filters=['*.py'])

    fs.create_file('/side1/anything')
    mocker.patch('zano.core.fs_nodes.Node.is_file', return_value=False)
    mocker.patch('zano.core.fs_nodes.Node.is_dir', return_value=False)
    assert not is_accepted(Node('/side1/anything'), filters=[])
    captured = capsys.readouterr()
    assert captured.out == \
        'IGNORING /side1/anything (neither a directory nor a file).\n'


def test_list_tree_structure(fs):
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    fs.create_dir('/side1/dir1/')
    fs.create_dir('/side1/dir1/dir3/')
    fs.create_dir('/side1/mydir3/')
    fs.create_dir('/side1/mydir3/renamed_on_side1')
    fs.create_dir('/side1/dir1/dir3/new')

    fs.create_dir('/side2/dir1/')
    fs.create_dir('/side2/dir1/dir3/')
    fs.create_dir('/side2/mydir3/')
    fs.create_dir('/side2/mydir3/original_name')
    fs.create_dir('/side2/dir1/dir3/new2')

    assert list_tree_structure('/side1', filters=['*/dir3']) \
        == ['dir1', 'mydir3', 'mydir3/renamed_on_side1']

    assert list_tree_structure('/side1', filters=['*dir3']) == ['dir1']

    assert list_tree_structure('/side2', filters=['*/dir3']) \
        == ['dir1', 'mydir3', 'mydir3/original_name']

    assert list_tree_structure('/side2', filters=['*dir3']) == ['dir1']


def test_probably_moved_file(fs, mocker):
    here = Node(__file__).parent

    # moved.txt on nomad has been moved from dirA to dirB
    n1 = Node(here / 'dirs/home/dirA/moved.txt',
              mask=str(here / 'dirs/home'))
    n2 = Node(here / 'dirs/nomad/dirB/moved.txt',
              mask=str(here / 'dirs/nomad'))
    f1 = fs.create_file(str(n1))
    f2 = fs.create_file(str(n2))

    m = mocker.patch('zano.core.fs_tools._check_consistency')
    m.return_value = True

    f1.st_mtime = 1630000700
    f2.st_mtime = 1630000700
    f1.st_ctime = 1630000700
    f2.st_ctime = 1630000800  # newer
    assert probably_moved(n1, n2) == 2
    assert probably_moved(n2, n1) == 1

    # different names
    n3 = Node(here / 'dirs/nomad/dirB/moved2.txt',
              mask=here / 'dirs/nomad')
    f3 = fs.create_file(str(n3))

    f3.st_mtime = 1630000700
    f3.st_ctime = 1630000900
    assert probably_moved(n1, n3) == 0
    assert probably_moved(n3, n1) == 0

    # different mtimes
    n4 = Node(here / 'dirs/home/dirA/moved_modified.txt',
              mask=here / 'dirs/home')
    n5 = Node(here / 'dirs/nomad/dirB/moved_modified.txt',
              mask=here / 'dirs/nomad')
    f4 = fs.create_file(str(n4))
    f5 = fs.create_file(str(n5))

    f4.st_mtime = 1626504391
    f5.st_mtime = 1626558805
    f4.st_ctime = 1626558778
    f5.st_ctime = 1626558806
    assert probably_moved(n4, n5) == 0


def test_moved_node(fs):
    d1 = fs.create_dir('/path/dir1')
    d1.st_ctime = 1626500000
    d2 = fs.create_dir('/path/dir2')
    d2.st_ctime = 1626500100
    d3 = fs.create_dir('/path2/dir2')
    d3.st_ctime = 1626500200
    f1 = fs.create_file('/path/file1')
    f1.st_ctime = 1626500000
    f2 = fs.create_file('/path/file2')
    f2.st_ctime = 1626500100
    f3 = fs.create_file('/path2/file2')
    f3.st_ctime = 1626500200
    assert moved_node(Node(d1.path), Node(d2.path)) == 0
    assert moved_node(Node(d2.path), Node(d1.path)) == 0
    assert moved_node(Node(d3.path), Node(d1.path)) == 1
    assert moved_node(Node(d1.path), Node(d3.path)) == 2
    assert moved_node(Node(d2.path), Node(d3.path)) == 2
    assert moved_node(Node(f1.path), Node(f2.path)) == 0
    assert moved_node(Node(f2.path), Node(f1.path)) == 0
    assert moved_node(Node(f3.path), Node(f1.path)) == 1
    assert moved_node(Node(f1.path), Node(f3.path)) == 2
    assert moved_node(Node(f2.path), Node(f3.path)) == 2


def test_renamed_node(fs):
    d1 = fs.create_dir('/path/dir1')
    d1.st_ctime = 1626500000
    d2 = fs.create_dir('/path/dir2')
    d2.st_ctime = 1626500100
    d3 = fs.create_dir('/path2/dir2')
    d3.st_ctime = 1626500200
    f1 = fs.create_file('/path/file1')
    f1.st_ctime = 1626500000
    f2 = fs.create_file('/path/file2')
    f2.st_ctime = 1626500100
    f3 = fs.create_file('/path2/file2')
    f3.st_ctime = 1626500200
    assert renamed_node(Node(d1.path), Node(d2.path)) == 2
    assert renamed_node(Node(d2.path), Node(d1.path)) == 1
    assert renamed_node(Node(d1.path), Node(d3.path)) == 0
    assert renamed_node(Node(f1.path), Node(f2.path)) == 2
    assert renamed_node(Node(f2.path), Node(f1.path)) == 1
    assert renamed_node(Node(f1.path), Node(f3.path)) == 0


def test_probably_renamed_file(fs, mocker):
    here = Node(__file__).parent

    n1 = Node(here / 'dirs/home/dirA/named.txt', mask=here / 'dirs/home')
    n2 = Node(here / 'dirs/nomad/dirA/renamed.txt', mask=here / 'dirs/nomad')
    n3 = Node(here / 'dirs/nomad/dirA/renamed2.txt', mask=here / 'dirs/nomad')
    f1 = fs.create_file(str(n1))
    f2 = fs.create_file(str(n2))
    f3 = fs.create_file(str(n3))

    m = mocker.patch('zano.core.fs_tools._check_consistency')
    m.return_value = True

    f1.st_mtime = 1626558007
    f2.st_mtime = 1626558007
    # Different mtimes should lead to not recognize the files as "probably"
    # renamed
    f3.st_mtime = 1626558220

    f1.st_ctime = 1626558007
    f2.st_ctime = 1626558039
    f3.st_ctime = 1626558220
    assert probably_renamed(n1, n2) == 2
    assert probably_renamed(n2, n1) == 1
    assert probably_renamed(n1, n3) == 0


def test_probably_moved_and_renamed_file(fs, mocker):
    here = Node(__file__).parent
    n1 = Node(here / 'dirs/home/dirA/named.txt', mask=here / 'dirs/home')
    n2 = Node(here / 'dirs/nomad/dirB/moved_and_renamed.txt',
              mask=here / 'dirs/nomad')
    n3 = Node(here / 'dirs/nomad/dirB/moved_renamed_modified.txt',
              mask=here / 'dirs/nomad')
    f1 = fs.create_file(str(n1))
    f2 = fs.create_file(str(n2))
    f3 = fs.create_file(str(n3))

    m = mocker.patch('zano.core.fs_tools._check_consistency')
    m.return_value = True

    f1.st_mtime = 1626558007
    f2.st_mtime = 1626558007
    f1.st_ctime = 1626558007
    f2.st_ctime = 1626558343  # newer
    assert probably_moved_and_renamed(n1, n2) == 2

    # Different mtimes should lead to not recognize the files as "probably"
    # renamed
    f3.st_mtime = 1626558583
    f3.st_ctime = 1626558583
    assert probably_moved_and_renamed(n1, n3) == 0


def test_probably_renamed_directory(fs, mocker):
    here = Node(__file__).parent

    m = mocker.patch('zano.core.fs_tools._check_consistency')
    m.return_value = True

    n1 = Node(here / 'dirs/home/dirD', mask=here / 'dirs/home')
    n2 = Node(here / 'dirs/nomad/dirD_renamed', mask=here / 'dirs/nomad')

    d1 = fs.create_dir(str(n1))
    d2 = fs.create_dir(str(n2))

    d1.st_mtime = 1627659537
    d2.st_mtime = 1627659537
    d1.st_ctime = 1627659537
    d2.st_ctime = 1627683342
    assert probably_renamed(n1, n2) == 2


def test_probably_moved_directory(fs, mocker):
    here = Node(__file__).parent

    m = mocker.patch('zano.core.fs_tools._check_consistency')
    m.return_value = True

    n1 = Node(here / 'dirs/home/dirD', mask=here / 'dirs/home')
    n2 = Node(here / 'dirs/nomad/dirA/dirD', mask=here / 'dirs/nomad')

    d1 = fs.create_dir(str(n1))
    d2 = fs.create_dir(str(n2))

    d1.st_mtime = 1627659537
    d2.st_mtime = 1627659537
    d1.st_ctime = 1627659537
    d2.st_ctime = 1627683478
    assert probably_moved(n1, n2) == 2


def test_probably_moved_and_renamed_directory(fs, mocker):
    here = Node(__file__).parent
    m = mocker.patch('zano.core.fs_tools._check_consistency')
    m.return_value = True

    n1 = Node(here / 'dirs/home/dirD', mask=here / 'dirs/home')
    n2 = Node(here / 'dirs/nomad/dirB/dirD_renamed', mask=here / 'dirs/nomad')

    d1 = fs.create_dir(str(n1))
    d2 = fs.create_dir(str(n2))

    d1.st_mtime = 1627659537
    d2.st_mtime = 1627659537
    d1.st_ctime = 1627659537
    d2.st_ctime = 1627683643
    assert probably_moved_and_renamed(n1, n2) == 2


def test_collect_dirs_inodes(fs):
    root3 = Node(__file__).parent / 'dirs/root3'

    n1 = root3 / 'dir1'
    n13 = root3 / 'dir1/dir13'
    n2 = root3 / 'dir2'

    d1 = fs.create_dir(str(n1))
    d13 = fs.create_dir(str(n13))
    d2 = fs.create_dir(str(n2))

    d1.st_ino = 101
    d13.st_ino = 102
    d2.st_ino = 103

    assert collect_dirs_inodes(root3) == [(101, Node('dir1')),
                                          (102, Node('dir1/dir13')),
                                          (103, Node('dir2'))]


def test_collect_paired_dirs_inodes(fs):
    root3 = '/root3'
    root4 = '/root4'
    dir0_3 = fs.create_dir('root3/dir0')
    dir0_3.st_ino = 19
    dir1_3 = fs.create_dir('root3/dir1')
    dir1_3.st_ino = 401
    dir1_4 = fs.create_dir('root4/dir1')
    dir1_4.st_ino = 402
    dir13_3 = fs.create_dir('root3/dir1/dir13')
    dir13_3.st_ino = 136
    dir13_4 = fs.create_dir('root4/dir1/dir13')
    dir13_4.st_ino = 137
    dir2_3 = fs.create_dir('root3/dir2')
    dir2_3.st_ino = 555
    dir2_4 = fs.create_dir('root4/dir2')
    dir2_4.st_ino = 888

    assert collect_paired_dirs_inodes(root3, root4) \
        == [(401, 402), (136, 137), (555, 888)]

    assert collect_paired_dirs_inodes(root3, root4, filters=['dir1/*']) \
        == [(401, 402), (555, 888)]

    assert collect_paired_dirs_inodes(root3, root4,
                                      filters=['dir1', 'dir1/*']) \
        == [(555, 888)]


def test_collect_newer_files(fs):
    here = Node(__file__).parent

    n1 = Node(here / 'dirs/root1')
    n2 = Node(here / 'dirs/root1/timestamp')
    n3 = Node(here / 'dirs/root1/modified')
    n4 = Node(here / 'dirs/root1/new')
    n5 = Node(here / 'dirs/root1/untouched')

    timestamp = fs.create_file(str(n2))
    modified = fs.create_file(str(n3))
    new = fs.create_file(str(n4))
    untouched = fs.create_file(str(n5))

    timestamp.st_mtime = 1627812185
    modified.st_mtime = 1627812383
    new.st_mtime = 1627812188
    untouched.st_mtime = 1627812154

    assert set(collect_newer_files(n1, ts=n2.st_mtime)) == {n3.relpath,
                                                            n4.relpath}

    fs.create_dir(str(n1 / 'dir1/dir2/dir3'))
    nf1 = n1 / 'dir1/newer1.py'
    newer1 = fs.create_file(str(nf1))
    newer1.st_mtime = 1630000000
    nf2 = n1 / 'dir1/dir2/newer2.pdf'
    newer2 = fs.create_file(str(nf2))
    newer2.st_mtime = 1630000000
    nf3 = n1 / 'dir1/dir2/dir3/newer3.txt'
    newer3 = fs.create_file(str(nf3))
    newer3.st_mtime = 1630000000
    of1 = n1 / 'dir1/older1'
    older1 = fs.create_file(str(of1))
    older1.st_mtime = 1620000000
    of2 = n1 / 'dir1/dir2/older2'
    older2 = fs.create_file(str(of2))
    older2.st_mtime = 1620000000
    of3 = n1 / 'dir1/dir2/dir3/older3'
    older3 = fs.create_file(str(of3))
    older3.st_mtime = 1620000000

    assert set(collect_newer_files(n1, ts=n2.st_mtime)) == {n3.relpath,
                                                            n4.relpath,
                                                            nf1.relpath,
                                                            nf2.relpath,
                                                            nf3.relpath}

    assert set(collect_newer_files(n1, ts=n2.st_mtime,
                                   filters=['dir1/*'])) == {n3.relpath,
                                                            n4.relpath}

    assert set(collect_newer_files(n1, ts=n2.st_mtime,
                                   filters=['dir2/*'])) == {n3.relpath,
                                                            n4.relpath,
                                                            nf1.relpath}

    assert set(collect_newer_files(n1, ts=n2.st_mtime,
                                   filters=['dir3/*'])) == {n3.relpath,
                                                            n4.relpath,
                                                            nf1.relpath,
                                                            nf2.relpath}

    assert set(collect_newer_files(n1, ts=n2.st_mtime,
                                   filters=['*.pdf'])) == {n3.relpath,
                                                           n4.relpath,
                                                           nf1.relpath,
                                                           nf3.relpath}


def test_collect_missing_files(fs):
    root1 = Node(__file__).parent / 'dirs/root1'
    root2 = Node(__file__).parent / 'dirs/root2'
    fs.create_dir(root1.path)
    fs.create_dir(root2.path)
    fs.create_file(str(root1 / 'new'))
    fs.create_file(str(root1 / 'modified'))
    fs.create_file(str(root2 / 'modified'))
    fs.create_file(str(root1 / 'timestamp'))
    fs.create_file(str(root2 / 'timestamp'))
    fs.create_file(str(root1 / 'untouched'))
    fs.create_file(str(root2 / 'untouched'))

    assert collect_missing_files(root1, root2) == [Node('new')]

    assert collect_missing_files(root1, root2, filters=['new']) == []

    fs.create_dir(str(root1 / 'sub1/sub3/sub5'))
    fs.create_dir(str(root2 / 'sub1/sub3/sub5'))
    fs.create_dir(str(root1 / 'sub2'))
    fs.create_dir(str(root2 / 'sub2'))

    fs.create_file(str(root1 / 'sub1/f1.odt'))
    fs.create_file(str(root2 / 'sub1/f1.odt'))
    fs.create_file(str(root1 / 'sub1/f2.txt'))
    fs.create_file(str(root2 / 'sub1/f2.txt'))

    fs.create_file(str(root1 / 'sub1/f4.txt'))

    fs.create_file(str(root2 / 'sub1/f3.txt'))

    fs.create_file(str(root1 / 'sub1/sub3/f6.odt'))
    fs.create_file(str(root2 / 'sub1/sub3/f6.odt'))
    fs.create_file(str(root1 / 'sub1/sub3/f6.txt'))
    fs.create_file(str(root2 / 'sub1/sub3/f6.txt'))

    fs.create_file(str(root1 / 'sub1/sub3/f7.txt'))

    fs.create_file(str(root2 / 'sub1/sub3/f8.txt'))

    fs.create_file(str(root1 / 'sub1/sub3/sub5/f9.odt'))
    fs.create_file(str(root2 / 'sub1/sub3/sub5/f9.odt'))
    fs.create_file(str(root1 / 'sub1/sub3/sub5/f9.py'))
    fs.create_file(str(root2 / 'sub1/sub3/sub5/f9.pdf'))
    fs.create_file(str(root1 / 'sub1/sub3/sub5/f9.txt'))
    fs.create_file(str(root2 / 'sub1/sub3/sub5/f9.txt'))

    fs.create_file(str(root1 / 'sub1/sub3/sub5/f10.py'))

    fs.create_file(str(root2 / 'sub1/sub3/sub5/f11.py'))

    fs.create_file(str(root1 / 'sub2/f1.odt'))
    fs.create_file(str(root2 / 'sub2/f1.odt'))
    fs.create_file(str(root1 / 'sub2/f2.txt'))
    fs.create_file(str(root2 / 'sub2/f2.txt'))

    fs.create_file(str(root1 / 'sub2/f1.pdf'))

    fs.create_file(str(root2 / 'sub2/f2.pdf'))

    assert collect_missing_files(root1, root2) \
        == [Node('new'), Node('sub1/f4.txt'), Node('sub1/sub3/f7.txt'),
            Node('sub1/sub3/sub5/f10.py'), Node('sub1/sub3/sub5/f9.py'),
            Node('sub2/f1.pdf')]

    assert collect_missing_files(root1, root2, filters=['sub1/*']) \
        == [Node('new'), Node('sub2/f1.pdf')]

    assert collect_missing_files(root1, root2, filters=['sub1/*', '*.pdf']) \
        == [Node('new')]

    assert collect_missing_files(root1, root2, filters=['*.txt']) \
        == [Node('new'), Node('sub1/sub3/sub5/f10.py'),
            Node('sub1/sub3/sub5/f9.py'), Node('sub2/f1.pdf')]

    assert collect_missing_files(root2, root1) \
        == [Node('sub1/f3.txt'), Node('sub1/sub3/f8.txt'),
            Node('sub1/sub3/sub5/f11.py'), Node('sub1/sub3/sub5/f9.pdf'),
            Node('sub2/f2.pdf')]

    assert collect_missing_files(root2, root1, filters=['sub5/*']) \
        == [Node('sub1/f3.txt'), Node('sub1/sub3/f8.txt'),
            Node('sub2/f2.pdf')]

    assert collect_missing_files(root2, root1, filters=['sub3/*']) \
        == [Node('sub1/f3.txt'), Node('sub2/f2.pdf')]


def test_collect_files_inodes(fs):
    root3 = Node(__file__).parent / 'dirs/root3'
    n13 = Node(root3 / 'dir1/dir13/f13.txt', mask=root3)
    n1 = Node(root3 / 'dir1/f1.txt', mask=root3)
    n2 = Node(root3 / 'dir2/f2.txt', mask=root3)

    f13 = fs.create_file(str(n13))
    f1 = fs.create_file(str(n1))
    f2 = fs.create_file(str(n2))
    f13.st_ino = 1001
    f1.st_ino = 1002
    f2.st_ino = 1003
    assert collect_files_inodes(root3) == [(1001, Node('dir1/dir13/f13.txt')),
                                           (1002, Node('dir1/f1.txt')),
                                           (1003, Node('dir2/f2.txt'))]

    n3 = Node(root3 / 'dir1/dir13/f13.py', mask=root3)
    n4 = Node(root3 / 'dir1/f1.py', mask=root3)
    n5 = Node(root3 / 'dir2/f2.py', mask=root3)

    f3 = fs.create_file(str(n3))
    f4 = fs.create_file(str(n4))
    f5 = fs.create_file(str(n5))
    f3.st_ino = 2003
    f4.st_ino = 2004
    f5.st_ino = 2005
    assert collect_files_inodes(root3, filters=['*.py']) \
        == [(1001, Node('dir1/dir13/f13.txt')),
            (1002, Node('dir1/f1.txt')),
            (1003, Node('dir2/f2.txt'))]
    assert collect_files_inodes(root3, filters=['*.txt']) \
        == [(2003, Node('dir1/dir13/f13.py')),
            (2004, Node('dir1/f1.py')),
            (2005, Node('dir2/f2.py'))]
    assert collect_files_inodes(root3, filters=['dir13/*', '*.txt']) \
        == [(2004, Node('dir1/f1.py')),
            (2005, Node('dir2/f2.py'))]


def test_collect_paired_files_inodes(fs):
    modified1 = fs.create_file('/root1/modified')
    new1 = fs.create_file('/root1/new')
    timestamp1 = fs.create_file('/root1/timestamp')
    untouched1 = fs.create_file('/root1/untouched')

    modified2 = fs.create_file('/root2/modified')
    timestamp2 = fs.create_file('/root2/timestamp')
    untouched2 = fs.create_file('/root2/untouched')

    modified1.st_ino = 61
    new1.st_ino = 62
    timestamp1.st_ino = 63
    untouched1.st_ino = 64

    modified2.st_ino = 31
    timestamp2.st_ino = 33
    untouched2.st_ino = 34

    assert collect_paired_files_inodes('/root1', '/root2') \
        == [(61, 31), (63, 33), (64, 34)]

    p1_1 = fs.create_file('/root1/dir1/p1.pdf')
    p1_1.st_ino = 100
    p1_2 = fs.create_file('/root2/dir1/p1.pdf')
    p1_2.st_ino = 101
    p2_1 = fs.create_file('/root1/dir1/dir2/p2.py')
    p2_1.st_ino = 200
    p2_2 = fs.create_file('/root2/dir1/dir2/p2.py')
    p2_2.st_ino = 201
    p3_1 = fs.create_file('/root1/dir1/dir2/dir3/p3.odt')
    p3_1.st_ino = 300
    p3_2 = fs.create_file('/root2/dir1/dir2/dir3/p3.odt')
    p3_2.st_ino = 301

    assert set(collect_paired_files_inodes('/root1', '/root2')) \
        == {(61, 31), (63, 33), (64, 34), (100, 101), (200, 201), (300, 301)}

    assert set(collect_paired_files_inodes('/root1', '/root2',
                                           filters=['dir1/*']))\
        == {(61, 31), (63, 33), (64, 34)}

    assert set(collect_paired_files_inodes('/root1', '/root2',
                                           filters=['*.py']))\
        == {(61, 31), (63, 33), (64, 34), (100, 101), (300, 301)}


def test_find_dir_matching_inode(fs):
    fs.create_dir('/side1/')
    dir4_1 = fs.create_dir('/side1/dir1/dir_/dir4renamed')
    dir4_1.st_ino = 1316331
    dir3_2 = fs.create_dir('/side2/dir1/dir3')
    dir3_2.st_ino = 1317994
    dir4_2 = fs.create_dir('/side2/dir2/dir4')
    dir4_2.st_ino = 1315093

    assert find_dir_matching_inode('/side1', 1316331) \
        == Node('dir1/dir_/dir4renamed')

    assert Node('/side2/dir2/dir4').st_ino == 1315093
    assert find_dir_matching_inode('/side2', 1315093) \
        == Node('dir2/dir4')

    root3 = Node(__file__).parent / 'dirs/root3'
    r3 = fs.create_dir(str(root3))
    d13 = fs.create_dir(str(root3 / 'dir1/dir13'))
    d2 = fs.create_dir(str(root3 / 'dir2'))
    r3.st_ino = 101
    d13.st_ino = 102
    d2.st_ino = 103

    assert find_dir_matching_inode(root3, 102) == Node('dir1/dir13')
    assert find_dir_matching_inode(root3, 104) is None


def test_find_file_matching_inode(fs, mocker):
    root3 = Node(__file__).parent / 'dirs/root3'
    n13 = root3 / 'dir1/dir13/f13.txt'
    n1 = root3 / 'dir1/f1.txt'
    n2 = root3 / 'dir2/f2.txt'
    f13 = fs.create_file(str(n13))
    f1 = fs.create_file(str(n1))
    f2 = fs.create_file(str(n2))
    f13.st_ino = 1001
    f1.st_ino = 1002
    f2.st_ino = 1003
    assert find_file_matching_inode(root3, 1003) == Node('dir2/f2.txt')
    assert find_file_matching_inode(root3, 1004) is None
    assert find_file_matching_inode(root3, 1003, filters=['*.txt']) is None
    assert find_file_matching_inode(root3, 1001, filters=['dir13/*']) is None
    assert find_file_matching_inode(root3, 1003, filters=['dir13/*'])\
        == Node('dir2/f2.txt')


def test_skim_missing_dirs(fs):
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    fs.create_dir('/side1/dir1')
    fs.create_dir('/side1/dir1/dir3')
    fs.create_dir('/side1/dir1/dir3/dir5')
    fs.create_dir('/side1/dir1/dir3/dir7')
    fs.create_dir('/side1/dir1/dir9')
    fs.create_dir('/side1/dir2')
    fs.create_dir('/side1/dir2/dir4')
    fs.create_dir('/side1/dir2/dir4/dir6')

    fs.create_dir('/side2/dir1')
    fs.create_dir('/side2/dir1/dir9')
    fs.create_dir('/side2/dir1/dir9/dir11')
    fs.create_dir('/side2/dir2')
    fs.create_dir('/side2/dir2/dir4')
    fs.create_dir('/side2/dir2/dir4/dir6')
    fs.create_dir('/side2/dir2/dir4/dir6/dir8')
    fs.create_dir('/side2/dir2/dir4/dir6/dir8/dir10')

    assert skim_missing_dirs('/side1', '/side2') \
        == [Node('dir1/dir3')]

    assert skim_missing_dirs('/side2', '/side1') \
        == [Node('dir1/dir9/dir11'), Node('dir2/dir4/dir6/dir8')]


def test_tree_structures_are_synced(fs):
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    fs.create_dir('/side1/dir1')
    fs.create_dir('/side1/dir1/dir3')
    fs.create_dir('/side1/dir1/dir3/dir5')
    fs.create_dir('/side1/dir1/dir3/dir7')
    fs.create_dir('/side1/dir1/dir9')
    fs.create_dir('/side1/dir2')
    fs.create_dir('/side1/dir2/dir4')
    fs.create_dir('/side1/dir2/dir4/dir6')

    fs.create_dir('/side2/dir1')
    fs.create_dir('/side2/dir1/dir9')
    fs.create_dir('/side2/dir1/dir9/dir11')
    fs.create_dir('/side2/dir2')
    fs.create_dir('/side2/dir2/dir4')
    fs.create_dir('/side2/dir2/dir4/dir6')
    fs.create_dir('/side2/dir2/dir4/dir6/dir8')
    fs.create_dir('/side2/dir2/dir4/dir6/dir8/dir10')

    assert not tree_structures_are_synced('/side1', '/side2')

    fs.create_dir('/side2/dir1/dir3/dir5')
    fs.create_dir('/side2/dir1/dir3/dir7')
    fs.create_dir('/side1/dir1/dir9/dir11')
    fs.create_dir('/side1/dir2/dir4/dir6/dir8/dir10')

    assert tree_structures_are_synced('/side1', '/side2')
