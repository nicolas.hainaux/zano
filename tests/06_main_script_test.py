# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import shutil
from pathlib import Path

from microlib import database
from click.testing import CliRunner
from pyfakefs.fake_filesystem_unittest import Pause

from zano import run, sync, pair
from zano.core.env import ZANO_LOCAL_SHARE

TESTDB_PATH = Path(__file__).parent / 'data/inodes.sqlite3'


def test_run():
    runner = CliRunner()
    result = runner.invoke(run, [])
    assert result.exit_code == 0


def test_pair(fs, mocker):
    runner = CliRunner()
    result = runner.invoke(pair, ['NONEXISTENT'])
    assert result.exit_code == 1

    runner = CliRunner()
    content = """[synced.side1]
name = 'SIDE1'
path = '/side1/'

[synced.side2]
name = 'SIDE2'
path = '/side2/'

[replicas]
"""
    fs.create_dir('/side1')
    fs.create_dir('/side2')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 2917
    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 7192
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    blank_db_path = Path(__file__).parent / 'data/blank.sqlite3'
    mocked_CM = database.ContextManager(blank_db_path)
    mocker.patch('microlib.database.ContextManager', return_value=mocked_CM)

    mocker.patch('zano.core.bundle.Bundle.create_idb')
    mocker.patch('zano.core.bundle.Bundle.set_timestamp')

    result = runner.invoke(pair, ['BUNDLE'])
    assert not result.exception
    assert result.exit_code == 0

    with database.ContextManager(blank_db_path) as cursor:
        idb = database.Operator(cursor)
        assert idb.get_table('files', include_headers=True) \
            == [('id', 'inode1', 'inode2'), ]
        assert idb.get_table('dirs', include_headers=True) \
            == [('id', 'inode1', 'inode2'),
                ('1', '2917', '7192'), ]

    with Pause(fs):
        blank_db = Path(__file__).parent / 'data/blank.sqlite3'
        blank_db.unlink()
        blank_db_copy = Path(__file__).parent / 'data/blank_copy.sqlite3'
        shutil.copy2(blank_db_copy, blank_db)


def test_sync_error():
    runner = CliRunner()
    result = runner.invoke(sync, ['NOBUNDLE'])
    assert str(result.output) == f'Error: Found no bundle named NOBUNDLE '\
        f'configured in {ZANO_LOCAL_SHARE}.\n'
    assert result.exit_code == 1
