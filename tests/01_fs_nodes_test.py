# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import subprocess
from pathlib import Path

import pytest

from zano.core.fs_nodes import Node


def test_Node(fs):
    fs.create_dir('/side1/dir1/root1/')
    f = fs.create_file('/side1/dir1/root1/path/to/file.txt',
                       contents='Some text')
    f.st_mtime = 162840000
    f.st_ctime = 162840010
    f.st_ino = 5689247
    n = Node('/side1/dir1/root1/path/to/file.txt',
             mask='/side1/dir1/root1/')
    assert n.relparent == Node('path/to')
    assert n.relpath == Node('path/to/file.txt')
    assert n.abspath == Node('/side1/dir1/root1/path/to/file.txt')
    assert n.st_mtime == 162840000
    assert n.st_ctime == 162840010
    assert n.st_ino == 5689247
    assert n.size == 9
    assert n.name == 'file.txt'

    assert str(n) == '/side1/dir1/root1/path/to/file.txt'

    n1 = Node(n)
    assert n1.mask == n.mask

    n1 = Node('/root/dir1/', mask='/root')
    n2 = Node('dir2/dir3')
    n3 = Node('dir4/dir5')
    n = Node(n1, n2, n3)
    assert n.mask == '/root'
    n = Node(n1, n2, n3, mask='/mask')
    assert n.mask == '/mask'
    n = Node('/root/dir1/', n2, n3)
    assert n.mask == '/'

    assert repr(n) == "Node('/root/dir1/dir2/dir3/dir4/dir5')"

    n = Node('/root/dir1/', n2, n3, mask='/root')
    assert repr(n) == "Node('[[/root]]/dir1/dir2/dir3/dir4/dir5')"

    n = Node('dir1/dir13/f13.txt')
    assert repr(n) == "Node('dir1/dir13/f13.txt')"
    assert n.mask == ''


def test_Node_errors(fs):
    fs.create_dir('/side1/dir1/root1/')
    fs.create_file('/side1/dir1/root1/path/to/file.txt',
                   contents='Some text')

    n = Node('/side2/dir1/root1/path/to/', mask='/side2/dir1/root1/')
    with pytest.raises(FileNotFoundError) as excinfo:
        n.size
    assert str(excinfo.value) == "[Errno 2] No such file or directory: "\
        "'/side2/dir1/root1/path/to'"

    n = Node('/side1/dir1/root1/path/to/', mask='/side1/dir1/root1/')
    with pytest.raises(IsADirectoryError) as excinfo:
        n.size
    assert str(excinfo.value) == "[Errno 21] Is a directory: "\
        "'/side1/dir1/root1/path/to'"


def test_Node_equality():
    n = Node('/side1/dir1/root1/path/to/file.txt',
             mask='/side1/dir1/root1/')
    assert n == Node('/side1/dir1/root1/path/to/file.txt',
                     mask='/side1/dir1/root1/')
    assert n != 89
    assert n != '/side1/dir1/root1/path/to/file.txt'
    assert n != Node('/side1/dir1/root1/path/to/file.txt')
    assert n != Path('/side1/dir1/root1/path/to/file.txt')

    n = Node('/side1/dir1/root1/path/to/file.txt')
    assert n == Path('/side1/dir1/root1/path/to/file.txt')


def test_Node_div():
    assert Node('/path/to') / 'my/file.txt' == Node('/path/to/my/file.txt')
    assert '/path/to' / Node('my/file.txt') == Node('/path/to/my/file.txt')


def test_Node_exists(fs):
    assert not Node('/side1').exists()
    fs.create_dir('/side1')
    assert Node('/side1').exists()


def test_Node_is_file(fs):
    assert not Node('/side1').is_file()
    fs.create_file('/side1')
    assert Node('/side1').is_file()


def test_Node_is_dir(fs):
    assert not Node('/side1').is_dir()
    fs.create_dir('/side1')
    assert Node('/side1').is_dir()
    assert not Node('/side1/file.txt').is_dir()


def test_real_file_st_ino():
    # file's st_ino is not kept as other properties by pyfs
    testfile = str(Node(__file__).parent / 'data/a_real_file.txt')
    f = Node(testfile, mask=Node(__file__).parent)
    # $ ls -i tests/dirs/a_real_file.txt
    # 1067156 tests/dirs/a_real_file.txt
    st_ino = subprocess.run(['ls', '-i', f'{str(testfile)}'],
                            capture_output=True, text=True).stdout
    assert f.st_ino == int(st_ino.split(' ')[0])


def test_real_file_properties(fs):
    testfile = str(Node(__file__).parent / 'data/a_real_file.txt')
    fs.add_real_file(testfile)
    f = Node(testfile, mask=Node(__file__).parent)
    assert f.name == 'a_real_file.txt'
    assert f.relpath == Node('data/a_real_file.txt')
    assert f.size == 13
    assert f.relparent == Node('data')
