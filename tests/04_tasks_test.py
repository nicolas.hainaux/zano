# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import shutil
from pathlib import Path

import pytest
from microlib import database
from pyfakefs.fake_filesystem import set_uid

from zano.core.env import ZANO_LOCAL_SHARE
from zano.core.bundle import Bundle
from zano.core.fs_nodes import Node
from zano.core.tasks import backup
from zano.core.tasks import retrieve_new_files_from_connected_replicas
from zano.core.tasks import push_changes_to_replicas
from zano.core.tasks import sync_tree_structures
from zano.core.tasks import sync_files
from zano.core.tasks import update_paired_files
from zano.core.tasks import PERMISSION_ERRORS_INFO

TESTDB_PATH = Path(__file__).parent / 'data/inodes.sqlite3'


def test_retrieve_new_files_from_connected_replicas(fs, mocker, capsys):
    content = """[synced.side1]
name = 'HOME1'
path = '/home1/'

[synced.side2]
name = 'HOME2'
path = '/home2/'

[replicas.CLG1]
path = '/drive1/'

[replicas.CLG2]
path = '/drive2/'
"""
    fs.create_dir('/home1/')
    fs.create_dir('/home2/')
    fs.create_dir('/drive1/')
    fs.create_file('/drive1/path/to/new1.txt')
    fs.create_file('/drive1/new2.txt')
    fs.create_dir('/drive2/')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')
    mocked_gt = mocker.patch('zano.core.bundle.Bundle.get_timestamp')
    mocked_cr = mocker.patch('zano.core.bundle.Bundle.connected_replicas')
    mocked_cr.side_effect = [{'CLG1': {'path': Node('/drive1/'),
                                       'filters': []},
                              'CLG2': {'path': Node('/drive2/'),
                                       'filters': []}
                              }]
    mocked_cn = mocker.patch('zano.core.tasks.collect_newer_files')
    mocked_cn.side_effect = [['path/to/new1.txt', 'new2.txt'], []]
    mocked_sc2 = mocker.patch('shutil.copy2')

    retrieve_new_files_from_connected_replicas(Bundle('BUNDLE'))
    mocked_gt.assert_called()
    mocked_cn.assert_called()
    captured = capsys.readouterr()
    assert captured.out == \
        'NEW FILES FOUND ON CLG1\n'\
        'path/to/new1.txt\n'\
        'new2.txt\n'
    mocked_sc2.assert_any_call(Path('/drive1/path/to/new1.txt'),
                               Path('/home1/path/to/new1.txt'))
    mocked_sc2.assert_any_call(Path('/drive1/path/to/new1.txt'),
                               Path('/home2/path/to/new1.txt'))
    mocked_sc2.assert_any_call(Path('/drive1/new2.txt'),
                               Path('/home1/new2.txt'))
    mocked_sc2.assert_called_with(Path('/drive1/new2.txt'),
                                  Path('/home2/new2.txt'))


def test_retrieve_new_files_from_connected_replicas_permission_errors(fs,
                                                                      mocker,
                                                                      capsys):
    content = """[synced.side1]
name = 'HOME1'
path = '/home1/'

[synced.side2]
name = 'HOME2'
path = '/home2/'

[replicas.CLG1]
path = '/drive1/'
"""
    set_uid(1000)
    h1 = fs.create_dir('/home1/')
    Node(h1.path).chmod(0o555)
    fs.create_dir('/home2/')
    fs.create_dir('/drive1/')
    nt = fs.create_file('/drive1/new.txt')
    with pytest.raises(PermissionError) as excinfo:
        shutil.copy2(nt.path, h1.path)
    assert str(excinfo.value) == "[Errno 13] Permission Denied: '/home1'"
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')
    mocker.patch('zano.core.bundle.Bundle.get_timestamp')
    mocked_cr = mocker.patch('zano.core.bundle.Bundle.connected_replicas')
    mocked_cr.side_effect = [{'CLG1': {'path': Node('/drive1/'),
                                       'filters': []}
                              }]
    mocked_cn = mocker.patch('zano.core.tasks.collect_newer_files')
    mocked_cn.side_effect = [['new.txt'], []]

    with pytest.raises(SystemExit) as excinfo:
        retrieve_new_files_from_connected_replicas(Bundle('BUNDLE'))
    assert str(excinfo.value) == '1'
    captured = capsys.readouterr()
    assert captured.out == \
        'NEW FILES FOUND ON CLG1\n'\
        'new.txt\n'\
        'Error: insufficient permissions to copy new.txt to HOME1 (/home1)'\
        f'{PERMISSION_ERRORS_INFO}\n'

    Node(h1.path).chmod(0o777)
    fs.create_file('/drive1/dir2/new2.txt')
    fs.create_dir('/home1/dir2')
    h2d2 = fs.create_dir('/home2/dir2')
    Node(h2d2.path).chmod(0o555)

    mocked_cr.side_effect = [{'CLG1': {'path': Node('/drive1/'),
                                       'filters': []}
                              }]
    mocked_cn.side_effect = [['dir2/new2.txt'], []]

    with pytest.raises(SystemExit) as excinfo:
        retrieve_new_files_from_connected_replicas(Bundle('BUNDLE'))
    assert str(excinfo.value) == '1'
    captured = capsys.readouterr()
    assert captured.out == \
        'NEW FILES FOUND ON CLG1\n'\
        'dir2/new2.txt\n'\
        'Error: insufficient permissions to copy new2.txt to HOME2 '\
        '(/home2/dir2)'\
        f'{PERMISSION_ERRORS_INFO}\n'


def test_backup(fs, mocker):
    content = """[synced.side1]
name = 'SIDE1'
path = '/side1/'
backup = '/back1'

[synced.side2]
name = 'SIDE2'
path = '/side2/'
backup = '/back2'
"""
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    fs.create_dir('/back1/')
    fs.create_dir('/back2/')
    fs.create_dir('/drive1/REP1/')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    mocked_sr = mocker.patch('zano.core.fs_tools.subprocess.run')
    b = Bundle('BUNDLE')
    backup(b, 'before')
    assert mocked_sr.call_count == 2
    mocked_sr.assert_any_call(
        'rsync -avzz --progress --delete --delete-excluded --delete-before '
        '--ignore-errors --force /side1/ /back1'.split())
    mocked_sr.assert_any_call(
        'rsync -avzz --progress --delete --delete-excluded --delete-before '
        '--ignore-errors --force /side2/ /back2'.split())


def test_push_changes_to_replicas(fs, mocker):
    mock_input = mocker.patch('builtins.input')
    content = """[synced.side1]
name = 'SIDE1'
path = '/side1/'

[synced.side2]
name = 'SIDE2'
path = '/side2/'

[replicas.REP1]
path = '/drive1/REP1/'
filters = 'ignored/path/'
"""
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    fs.create_dir('/drive1/REP1/')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    mocker.patch('zano.core.bundle.Bundle.set_timestamp')
    mocked_cr = mocker.patch('zano.core.bundle.Bundle.connected_replicas')
    mocked_cr.side_effect = [{'REP1': {'path': Node('/drive1/REP1/'),
                                       'filters': ['ignored/path/']}
                              }]
    mocked_sr = mocker.patch('zano.core.fs_tools.subprocess.run')
    b = Bundle('BUNDLE')
    mock_input.side_effect = ['Y']
    push_changes_to_replicas(b)
    mocked_sr.assert_called_with(['rsync', '-rtv', '--delete',
                                  '--delete-excluded', '--force',
                                  '--modify-window=2',
                                  '--filter', '- ignored/path/',
                                  '/side1/', '/drive1/REP1'])


def test_sync_tree_structures_no_change(fs, capsys):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 1316923
    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 1317712

    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'No change\n'


def test_sync_tree_structures_new_dirs(fs, capsys):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1 = fs.create_dir('/side1/dir1')
    dir1.st_ino = 3063233  # does not exist in test db
    dir2 = fs.create_dir('/side2/dir2')
    dir2.st_ino = 3060001  # does not exist in test db
    dir3 = fs.create_dir('/side2/dir2/dir3')
    dir3.st_ino = 3060003  # does not exist in test db
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        assert Node('/side2/dir1').is_dir()
        inode2 = Node('/side2/dir1').st_ino
        assert b.get_inode_matching('dirs', 'inode1', 3063233) == inode2

        assert Node('/side1/dir2').is_dir()
        inode1 = Node('/side1/dir2').st_ino
        assert b.get_inode_matching('dirs', 'inode2', 3060001) == inode1

        assert Node('/side1/dir2/dir3').is_dir()
        inode1 = Node('/side1/dir2/dir3').st_ino
        assert b.get_inode_matching('dirs', 'inode2', 3060003) == inode1

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: NEW dir1\n'\
            'HOME: NEW dir2\n'\
            'HOME: NEW dir2/dir3\n'


def test_sync_tree_structures_renamed_dirs(fs, capsys):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 1316923
    dir2_1 = fs.create_dir('/side1/dir2')
    dir2_1.st_ino = 1316924
    dir3_1 = fs.create_dir('/side1/dir2/dir3')
    dir3_1.st_ino = 1317987
    dir3_1.st_mtime = 1600000000
    dir3_1.st_ctime = 1600000100
    dir4_1 = fs.create_dir('/side1/dir1/dir4_renamed')
    dir4_1.st_ino = 1316331
    dir4_1.st_mtime = 1600000000
    dir4_1.st_ctime = 1600000200  # most recently renamed (on side1)

    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 1317712
    dir2_2 = fs.create_dir('/side2/dir2')
    dir2_2.st_ino = 1317969
    dir3_2 = fs.create_dir('/side2/dir2/dir3_renamed')
    dir3_2.st_ino = 1317994
    dir3_2.st_mtime = 1600000000
    dir3_2.st_ctime = 1600000200  # most recently renamed (on side2)
    dir4_2 = fs.create_dir('/side2/dir1/dir4')
    dir4_2.st_ino = 1315093
    dir4_2.st_mtime = 1600000000
    dir4_2.st_ctime = 1600000100

    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        assert Node('/side1/dir2/dir3_renamed').is_dir()
        assert Node('/side2/dir1/dir4_renamed').is_dir()
        assert not Node('/side1/dir2/dir3').is_dir()
        assert not Node('/side2/dir1/dir4').is_dir()

        assert Node('/side1/dir2/dir3_renamed').st_ino == 1317987
        assert b.get_inode_matching('dirs', 'inode1', 1317987) == 1317994
        assert Node('/side2/dir1/dir4_renamed').st_ino == 1315093
        assert b.get_inode_matching('dirs', 'inode2', 1315093) == 1316331

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: RENAME dir1/dir4 AS dir1/dir4_renamed\n'\
            'HOME: RENAME dir2/dir3 AS dir2/dir3_renamed\n'


def test_sync_tree_structures_moved_dirs(fs, capsys):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 1316923
    dir2_1 = fs.create_dir('/side1/dir2')
    dir2_1.st_ino = 1316924
    dir3_1 = fs.create_dir('/side1/dir2/dir3')
    dir3_1.st_ino = 1317987
    dir3_1.st_mtime = 1600000000
    dir3_1.st_ctime = 1600000100
    dir4_1 = fs.create_dir('/side1/dir1/dir_/dir4renamed')
    dir4_1.st_ino = 1316331
    dir4_1.st_mtime = 1600000000
    dir4_1.st_ctime = 1600000200

    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 1317712
    dir3_2 = fs.create_dir('/side2/dir1/dir3')
    dir3_2.st_ino = 1317994
    dir3_2.st_mtime = 1600000000
    dir3_2.st_ctime = 1600000200  # most recent move (on side2)
    dir2_2 = fs.create_dir('/side2/dir2')
    dir2_2.st_ino = 1317969
    fs.create_dir('/side2/dir1/dir_')
    dir4_2 = fs.create_dir('/side2/dir2/dir4')
    dir4_2.st_ino = 1315093
    dir4_2.st_mtime = 1600000000
    dir4_2.st_ctime = 1600000100

    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        assert Node('/side1/dir1/dir3').is_dir()
        assert Node('/side1/dir1/dir3').st_ino == 1317987
        assert not Node('/side1/dir2/dir3').is_dir()
        assert Node('/side2/dir1/dir3').is_dir()
        assert not Node('/side2/dir2/dir3').is_dir()

        assert Node('/side2/dir1/dir_/dir4renamed').st_ino \
            == 1315093
        assert not Node('/side2/dir2/dir4').is_dir()
        assert not Node('/side1/dir2/dir3').is_dir()

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: MOVE dir2/dir4 TO dir1/dir_/dir4renamed\n'\
            'HOME: MOVE dir2/dir3 TO dir1/dir3\n'


def test_sync_tree_structures_deleted_dirs(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    ts = """synced = 1640000500
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml', contents=ts)

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 1316923
    dir2_1 = fs.create_dir('/side1/dir2')   # deleted on side2
    dir2_1.st_ino = 1316924
    dir3_1 = fs.create_dir('/side1/dir2/dir3')
    dir3_1.st_ino = 1317987
    dir6_1 = fs.create_dir('/side1/dir1/DIR6')
    dir6_1.st_ino = 1320001
    dir6_1.st_ctime = 1640000900
    dir7_1 = fs.create_dir('/side1/dir1/DIR7')
    dir7_1.st_ino = 1320003
    dir7_1.st_ctime = 1640000900

    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 1317712
    dir4_2 = fs.create_dir('/side2/dir1/dir4')   # deleted on side1
    dir4_2.st_ino = 1315093
    dir5_2 = fs.create_dir('/side2/dir5')   # deleted on side1
    dir5_2.st_ino = 1320006
    dir6_2 = fs.create_dir('/side2/dir5/dir6')  # moved on side1
    dir6_2.st_ino = 1320002
    dir6_2.st_ctime = 1640000600
    dir7_2 = fs.create_dir('/side2/dir5/dir7')  # moved on side1
    dir7_2.st_ino = 1320004
    dir7_2.st_ctime = 1640000600
    dir8_2 = fs.create_dir('/side2/dir5/dir8')  # NOT moved on side1
    dir8_2.st_ino = 1320008

    def fake_send2trash(arg):
        fs.remove_object(arg)

    mocker.patch('zano.core.tasks.send2trash', side_effect=fake_send2trash)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        assert not Node('/side1/dir2/dir3').is_dir()
        assert not Node('/side1/dir2').is_dir()
        assert not Node('/side2/dir1/dir4').is_dir()
        assert not Node('/side2/dir5/dir8').is_dir()

        assert not b.is_known_inode('dirs', 'inode1', 1316924)
        assert not b.is_known_inode('dirs', 'inode2', 1315093)

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: MOVE dir5/dir6 TO dir1/DIR6\n'\
            'NOMAD: MOVE dir5/dir7 TO dir1/DIR7\n'\
            'HOME: DELETE dir2\n'\
            'NOMAD: DELETE dir1/dir4\n'\
            'NOMAD: DELETE dir5\n'


def test_sync_tree_structures_rescue_files_in_deleted_dirs(fs, mocker, capsys):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    ts = """synced = 1640000500
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml', contents=ts)

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 1316923
    dir2_1 = fs.create_dir('/side1/dir2')   # deleted on side2
    dir2_1.st_ino = 1316924
    dir3_1 = fs.create_dir('/side1/dir2/dir3')
    dir3_1.st_ino = 1317987
    dir6_1 = fs.create_dir('/side1/dir1/DIR6')
    dir6_1.st_ino = 1320001
    dir6_1.st_ctime = 1640000900
    dir7_1 = fs.create_dir('/side1/dir1/DIR7')
    dir7_1.st_ino = 1320003
    dir7_1.st_ctime = 1640000900

    f1_1 = fs.create_file('/side1/dir2/f1.txt')   # moved on side2
    f1_1.st_ino = 1067612
    f1_2 = fs.create_file('/side1/dir1/DIR7/f2.txt')   # moved on side1
    f1_2.st_ino = 1067502

    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 1317712
    dir4_2 = fs.create_dir('/side2/dir1/dir4')   # deleted on side1
    dir4_2.st_ino = 1315093
    dir5_2 = fs.create_dir('/side2/dir5')   # deleted on side1
    dir5_2.st_ino = 1320006
    dir6_2 = fs.create_dir('/side2/dir5/dir6')  # moved on side1
    dir6_2.st_ino = 1320002
    dir6_2.st_ctime = 1640000600
    dir7_2 = fs.create_dir('/side2/dir5/dir7')  # moved on side1
    dir7_2.st_ino = 1320004
    dir7_2.st_ctime = 1640000600
    dir8_2 = fs.create_dir('/side2/dir5/dir8')  # NOT moved on side1
    dir8_2.st_ino = 1320008

    f1_2 = fs.create_file('/side2/dir1/f1.txt')   # moved on side2
    f1_2.st_ino = 1065664
    f2_2 = fs.create_file('/side2/dir1/dir4/f2.txt')   # moved on side1
    f2_2.st_ino = 1067962

    def fake_send2trash(arg):
        fs.remove_object(arg)

    mocker.patch('zano.core.tasks.send2trash', side_effect=fake_send2trash)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        assert not Node('/side1/dir2/dir3').is_dir()
        assert not Node('/side1/dir2').is_dir()
        assert not Node('/side2/dir1/dir4').is_dir()
        assert not Node('/side2/dir5/dir8').is_dir()
        assert Node('/side1/dir1/f1.txt').is_file()
        assert Node('/side2/dir1/f1.txt').is_file()
        assert Node('/side1/dir1/DIR7/f2.txt').is_file()
        assert Node('/side2/dir1/DIR7/f2.txt').is_file()

        assert not b.is_known_inode('dirs', 'inode1', 1316924)
        assert not b.is_known_inode('dirs', 'inode2', 1315093)

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: MOVE dir5/dir6 TO dir1/DIR6\n'\
            'NOMAD: MOVE dir5/dir7 TO dir1/DIR7\n'\
            'HOME: MOVE dir2/f1.txt TO dir1/f1.txt\n'\
            'HOME: DELETE dir2\n'\
            'NOMAD: MOVE dir1/dir4/f2.txt TO dir1/DIR7/f2.txt\n'\
            'NOMAD: DELETE dir1/dir4\n'\
            'NOMAD: DELETE dir5\n'


def test_sync_tree_structures_rescue_files_permission_errors(fs,
                                                             capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    ts = """synced = 1640000500
"""
    set_uid(1000)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml', contents=ts)

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')
    dir1_1 = fs.create_dir('/side1/dir1')
    dir1_1.st_ino = 1316923
    Node(dir1_1.path).chmod(0o555)  # remove write permissions
    dir2_1 = fs.create_dir('/side1/dir2')   # deleted on side2
    dir2_1.st_ino = 1316924

    f1_1 = fs.create_file('/side1/dir2/f1.txt')   # moved on side2
    f1_1.st_ino = 1067612

    dir1_2 = fs.create_dir('/side2/dir1')
    dir1_2.st_ino = 1317712

    f1_2 = fs.create_file('/side2/dir1/f1.txt')   # moved on side2
    f1_2.st_ino = 1065664

    try:
        shutil.move(Node('/side1/dir2/f1.txt').path,
                    Node('/side1/dir1/f1.txt').path)
    except PermissionError:
        pass

    def fake_send2trash(arg):
        fs.remove_object(arg)

    mocker.patch('zano.core.tasks.send2trash', side_effect=fake_send2trash)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)

        with pytest.raises(SystemExit) as excinfo:
            sync_tree_structures(b)
        assert str(excinfo.value) == '1'

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'HOME: MOVE dir2/f1.txt TO dir1/f1.txt\n'\
            'Error: insufficient permissions to move dir2/f1.txt to '\
            'dir1/f1.txt on HOME'\
            f'{PERMISSION_ERRORS_INFO}\n'


def test_sync_tree_structures_scenario_01(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    k1_1 = fs.create_dir('/side1/dir1/kept1')
    k1_1.st_ino = 1316924
    m1_1 = fs.create_dir('/side1/dir1/moved_on_side1')
    m1_1.st_ino = 1316331
    m1_1.st_ctime = 1600000200
    fs.create_dir('/side1/dir2')

    d1_2 = fs.create_dir('/side2/dir1')
    d1_2.st_ino = 1317712
    k1_2 = fs.create_dir('/side2/dir1/kept1')
    k1_2.st_ino = 1317969
    dd_1_2 = fs.create_dir('/side2/dir1/deleted_on_side1')
    dd_1_2.st_ino = 1317994
    n_1_2 = fs.create_dir('/side2/dir1/deleted_on_side1/new_on_side2')
    n_1_2.st_ino = 1316666
    fs.create_dir('/side2/dir2')
    m1_2 = fs.create_dir('/side2/dir2/moved_on_side1')
    m1_2.st_ino = 1315093
    m1_2.st_ctime = 1600000100
    n_2_2 = fs.create_dir('/side2/dir2/moved_on_side1/new_on_side2')
    n_2_2.st_ino = 1315555

    def fake_send2trash(arg):
        fs.remove_object(arg)

    mocker.patch('zano.core.tasks.send2trash', side_effect=fake_send2trash)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        assert not Node('/side2/dir1/deleted_on_side1').is_dir()
        assert not Node('/side2/dir2/moved_on_side1').is_dir()
        assert Node('/side2/dir1/moved_on_side1').is_dir()
        assert Node('/side1/dir1/moved_on_side1/new_on_side2').is_dir()

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: MOVE dir2/moved_on_side1 TO dir1/moved_on_side1\n'\
            'NOMAD: CAUTION: found NEW directory dir1/deleted_on_side1/'\
            'new_on_side2 in to-be-deleted directory dir1/deleted_on_side1; '\
            'you might want to get it back from the trashcan...\n'\
            'NOMAD: DELETE dir1/deleted_on_side1\n'\
            'HOME: NEW dir1/moved_on_side1/new_on_side2\n'


def test_sync_tree_structures_scenario_02(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d1_1.st_ctime = 1640000500
    d2_1 = fs.create_dir('/side1/dir2/')
    d2_1.st_ino = 1316924
    d2_1.st_ctime = 1640000400
    d3_1 = fs.create_dir('/side1/dir1/DIR3/')
    d3_1.st_ino = 1317987
    d3_1.st_ctime = 1640000800
    d4_1 = fs.create_dir('/side1/DIR4')
    d4_1.st_ino = 1316331
    d4_1.st_ctime = 1640000700
    d6_1 = fs.create_dir('/side1/dir1/DIR3/dir6')  # deleted
    d6_1.st_ino = 1320001
    d6_1.st_ctime = 1640000800

    d1_2 = fs.create_dir('/side2/DIR1/')
    d1_2.st_ino = 1317712
    d1_2.st_ctime = 1640000600
    d2_2 = fs.create_dir('/side2/DIR1/DIR2/')
    d2_2.st_ino = 1317969
    d2_2.st_ctime = 1640000700
    d3_2 = fs.create_dir('/side2/dir3/')
    d3_2.st_ino = 1317994
    d3_2.st_ctime = 1640000400
    d4_2 = fs.create_dir('/side2/dir3/dir4')
    d4_2.st_ino = 1315093
    d4_2.st_ctime = 1640000200
    d5_2 = fs.create_dir('/side2/dir3/dir4/dir5')  # new
    d5_2.st_ino = 1360002
    d5_2.st_ctime = 1640000900

    def fake_send2trash(arg):
        fs.remove_object(arg)

    mocker.patch('zano.core.tasks.send2trash', side_effect=fake_send2trash)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        assert Node('/side1/DIR1/DIR3').is_dir()
        assert Node('/side1/DIR1/DIR2').is_dir()
        assert Node('/side1/DIR4/dir5').is_dir()

        assert not Node('/side1/DIR1/DIR3/dir6').is_dir()
        assert not Node('/side1/dir1').is_dir()
        assert not Node('/side1/dir2').is_dir()
        assert not Node('/side1/dir3').is_dir()

        assert Node('/side2/DIR1/DIR3').is_dir()
        assert Node('/side2/DIR1/DIR2').is_dir()
        assert Node('/side2/DIR4/dir5').is_dir()

        assert not Node('/side2/DIR1/DIR3/dir6').is_dir()
        assert not Node('/side2/dir1').is_dir()
        assert not Node('/side2/dir2').is_dir()
        assert not Node('/side2/dir3').is_dir()

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: MOVE dir3/dir4 TO DIR4\n'\
            'HOME: RENAME dir1 AS DIR1\n'\
            'HOME: MOVE dir2 TO DIR1/DIR2\n'\
            'HOME: NEW DIR4/dir5\n'\
            'NOMAD: MOVE dir3 TO DIR1/DIR3\n'\
            'HOME: DELETE DIR1/DIR3/dir6\n'


def test_sync_tree_structures_permission_error_on_new_dir(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    set_uid(1000)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    # remove write permission on dir1/ does prevent to create dir1/new/
    Node(d1_1.path).chmod(0o555)

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    n1_2 = fs.create_dir('/side2/dir1/new')
    n1_2.st_ino = 1310006  # does not exist on side1

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        with pytest.raises(SystemExit) as excinfo:
            sync_tree_structures(b)
        assert str(excinfo.value) == '1'

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'HOME: NEW dir1/new\n'\
            'Error: insufficient permissions to create /side1/dir1/new'\
            f'{PERMISSION_ERRORS_INFO}\n'


def test_sync_tree_structures_permission_error_on_deleted_dir(fs, capsys,
                                                              mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    set_uid(1000)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir1/dir2/')   # deleted on side2
    d2_1.st_ino = 1316924
    Node(d1_1.path).chmod(0o555)  # prevents to delete dir2/

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712

    def fake_send2trash(arg):
        raise PermissionError

    mocker.patch('zano.core.tasks.send2trash', side_effect=fake_send2trash)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        with pytest.raises(SystemExit) as excinfo:
            sync_tree_structures(b)
        assert str(excinfo.value) == '1'

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'HOME: DELETE dir1/dir2\n'\
            'Error: insufficient permissions to move /side1/dir1/dir2 to '\
            'trash'\
            f'{PERMISSION_ERRORS_INFO}\n'


def test_sync_tree_structures_permission_error_on_moved_dir(fs, capsys,
                                                            mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    set_uid(1000)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923

    d2_1 = fs.create_dir('/side1/dir1/dir2/')  # renamed on side2
    d2_1.st_ino = 1316924
    d2_1.st_ctime = 1600000200

    Node(d1_1.path).chmod(0o555)  # prevents to rename dir2/

    with pytest.raises(PermissionError) as excinfo:
        shutil.move(Path('/side1/dir1/dir2'),
                    Path('/side1/dir1/dir3'))
    assert str(excinfo.value) \
        == "[Errno 13] Permission Denied: '/side1/dir1'"

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir1/dir3/')
    d2_2.st_ino = 1317969
    d2_2.st_ctime = 1600000400

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        with pytest.raises(SystemExit) as excinfo:
            sync_tree_structures(b)
        assert str(excinfo.value) == '1'

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'HOME: RENAME dir1/dir2 AS dir1/dir3\n'\
            'Error: insufficient permissions to rename dir1/dir2 as '\
            f'dir1/dir3 on HOME{PERMISSION_ERRORS_INFO}\n'


def test_sync_tree_structures_filters(fs, capsys, mocker):
    content = """[synced]
filters = ['*/dir3']

[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir1/dir3/')
    d2_1.st_ino = 1316924
    d3_1 = fs.create_dir('/side1/mydir3/')
    d3_1.st_ino = 1317987
    d4_1 = fs.create_dir('/side1/mydir3/renamed_on_side1')
    d4_1.st_ino = 1316331
    d4_1.st_ctime = 1600000200
    d5_1 = fs.create_dir('/side1/dir1/dir3/new')
    d5_1.st_ino = 1310056

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir1/dir3/')
    d2_2.st_ino = 1317969
    d3_2 = fs.create_dir('/side2/mydir3/')
    d3_2.st_ino = 1317994
    d4_2 = fs.create_dir('/side2/mydir3/original_name')
    d4_2.st_ino = 1315093
    d4_2.st_ctime = 1600000000
    d5_2 = fs.create_dir('/side2/dir1/dir3/new2')
    d5_2.st_ino = 1310028

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: RENAME mydir3/original_name AS mydir3/renamed_on_side1\n'

    content = """[synced]
filters = ['*dir3']

[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.remove_object(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        captured = capsys.readouterr()
        assert captured.out == 'SYNCING DIRECTORIES\n' \
            'No change\n'


def test_sync_tree_structures_git_protection(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir1/new/')
    d2_1.st_ino = 1310001
    d3_1 = fs.create_dir('/side1/repo/')
    d3_1.st_ino = 1310055
    d4_1 = fs.create_dir('/side1/repo/.git')
    d4_1.st_ino = 1310056
    d4_1 = fs.create_dir('/side1/repo/sub')
    d4_1.st_ino = 1310057

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir1/new2/')
    d2_2.st_ino = 1310002
    d3_2 = fs.create_dir('/side2/repo2/')
    d3_2.st_ino = 1310010
    d4_2 = fs.create_dir('/side2/repo2/.git')
    d4_2.st_ino = 1310011
    d4_2 = fs.create_dir('/side2/repo2/sub')
    d4_2.st_ino = 1310012

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_tree_structures(b)

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'NOMAD: NEW dir1/new\n'\
            'HOME: NEW dir1/new2\n'


def test_sync_tree_structures_impossible_case(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'

[replicas]
"""
    set_uid(1000)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923

    d1_2 = fs.create_dir('/side2/DIR1/')
    d1_2.st_ino = 1317712

    b = Bundle('BUNDLE')

    mocker.patch('zano.core.tasks.moved_node', return_value=0)
    mocker.patch('zano.core.tasks.renamed_node', return_value=0)

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        with pytest.raises(SystemExit) as excinfo:
            sync_tree_structures(b)
        assert str(excinfo.value) == '1'

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING DIRECTORIES\n'\
            'Error: found matching nodes: /side1/dir1 and /side2/DIR1, '\
            'but impossible to find out which one is the moved or renamed '\
            'version of the other. Please perform the correction and run '\
            'zano again.\n'


def test_update_paired_files(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    mocker.patch('zano.core.bundle.Bundle.get_timestamp',
                 return_value=1600000200)

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir2/')
    d2_1.st_ino = 1316924
    f1_1 = fs.create_file('/side1/dir1/f1.txt')
    f1_1.st_ino = 1067612
    f1_1.st_mtime = 1600000300
    f2_1 = fs.create_file('/side1/dir2/f2.txt')
    f2_1.st_ino = 1067502
    f2_1.st_mtime = 1600000100
    f3_1 = fs.create_file('/side1/f3.txt')
    f3_1.st_ino = 1067277
    f3_1.st_mtime = 1600000050

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir2/')
    d2_2.st_ino = 1317969
    f1_2 = fs.create_file('/side2/dir1/f1.txt')
    f1_2.st_ino = 1065664
    f1_2.st_mtime = 1600000100
    f2_2 = fs.create_file('/side2/dir2/f2.txt')
    f2_2.st_ino = 1067962
    f2_2.st_mtime = 1600000400
    f3_2 = fs.create_file('/side2/f3.txt')
    f3_2.st_ino = 1067963
    f3_2.st_mtime = 1600000050

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        result = update_paired_files(b)
        assert result is True

        captured = capsys.readouterr()
        assert captured.out == \
            'NOMAD: UPDATE dir1/f1.txt\n'\
            'HOME: UPDATE dir2/f2.txt\n'

        assert Node(f1_2.path).st_mtime == 1600000300
        assert Node(f1_2.path).st_mtime == Node(f1_1.path).st_mtime
        assert Node(f2_1.path).st_mtime == 1600000400
        assert Node(f2_1.path).st_mtime == Node(f2_2.path).st_mtime


def test_update_paired_files_conflicts(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    mocker.patch('zano.core.bundle.Bundle.get_timestamp',
                 return_value=1600000200)

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir2/')
    d2_1.st_ino = 1316924
    f1_1 = fs.create_file('/side1/dir1/f1.txt')
    f1_1.st_ino = 1067612
    f1_1.st_mtime = 1600000300  # both mtimes > timestamp: conflict
    f2_1 = fs.create_file('/side1/dir2/f2.txt')
    f2_1.st_ino = 1067502
    f2_1.st_mtime = 1600000100
    f3_1 = fs.create_file('/side1/f3.txt')
    f3_1.st_ino = 1067277
    f3_1.st_mtime = 1600000050

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir2/')
    d2_2.st_ino = 1317969
    f1_2 = fs.create_file('/side2/dir1/f1.txt')
    f1_2.st_ino = 1065664
    f1_2.st_mtime = 1600000400  # both mtimes > timestamp: conflict
    f2_2 = fs.create_file('/side2/dir2/f2.txt')
    f2_2.st_ino = 1067962
    f2_2.st_mtime = 1600000400
    f3_2 = fs.create_file('/side2/f3.txt')
    f3_2.st_ino = 1067963
    f3_2.st_mtime = 1600000050

    b = Bundle('BUNDLE')

    mock_input = mocker.patch('builtins.input')
    mock_input.side_effect = ['NOMAD']

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        result = update_paired_files(b)
        assert result is True

        assert mock_input.called_with(
            'dir1/f1.txt has been modified on both sides '
            '(most recently modified on NOMAD). '
            'Do you want to keep the version of HOME or NOMAD?')

        captured = capsys.readouterr()
        assert captured.out == \
            'HOME: UPDATE dir1/f1.txt\n'\
            'HOME: UPDATE dir2/f2.txt\n'

        assert Node(f1_2.path).st_mtime == 1600000400
        assert Node(f1_2.path).st_mtime == Node(f1_1.path).st_mtime
        assert Node(f2_1.path).st_mtime == 1600000400
        assert Node(f2_1.path).st_mtime == Node(f2_2.path).st_mtime


def test_update_paired_files_no_update(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    mocker.patch('zano.core.bundle.Bundle.get_timestamp',
                 return_value=1600000200)

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir2/')
    d2_1.st_ino = 1316924
    f1_1 = fs.create_file('/side1/dir1/f1.txt')
    f1_1.st_ino = 1067612
    f1_1.st_mtime = 1600000100
    f2_1 = fs.create_file('/side1/dir2/f2.txt')
    f2_1.st_ino = 1067502
    f2_1.st_mtime = 1600000100
    f3_1 = fs.create_file('/side1/f3.txt')
    f3_1.st_ino = 1067277
    f3_1.st_mtime = 1600000050

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir2/')
    d2_2.st_ino = 1317969
    f1_2 = fs.create_file('/side2/dir1/f1.txt')
    f1_2.st_ino = 1065664
    f1_2.st_mtime = 1600000100
    f2_2 = fs.create_file('/side2/dir2/f2.txt')
    f2_2.st_ino = 1067962
    f2_2.st_mtime = 1600000100
    f3_2 = fs.create_file('/side2/f3.txt')
    f3_2.st_ino = 1067963
    f3_2.st_mtime = 1600000050

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        assert not update_paired_files(b)

        captured = capsys.readouterr()
        assert captured.out == ''


def test_sync_files(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    mocker.patch('zano.core.bundle.Bundle.get_timestamp',
                 return_value=1600000200)

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir2/')
    d2_1.st_ino = 1316924
    f1_1 = fs.create_file('/side1/dir1/f1.txt')
    f1_1.st_ino = 1067612
    f1_1.st_mtime = 1600000300
    f2_1 = fs.create_file('/side1/dir2/f2.txt')
    f2_1.st_ino = 1067502
    f2_1.st_mtime = 1600000100
    f3_1 = fs.create_file('/side1/f3.txt')
    f3_1.st_ino = 1067277
    f3_1.st_mtime = 1600000050
    f4_1 = fs.create_file('/side1/deleted.txt')
    f4_1.st_ino = 1318315
    f4_1.st_mtime = 1600000050
    f6_1 = fs.create_file('/side1/moved.txt')
    f6_1.st_ino = 1317970
    f6_1.st_ctime = 1600000700
    f6_1.st_mtime = 1600000050
    f7_1 = fs.create_file('/side1/dir2/renamed.txt')
    f7_1.st_ino = 1318030
    f7_1.st_ctime = 1600000700
    f7_1.st_mtime = 1600000060

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir2/')
    d2_2.st_ino = 1317969
    f1_2 = fs.create_file('/side2/dir1/f1.txt')
    f1_2.st_ino = 1065664
    f1_2.st_mtime = 1600000100
    f2_2 = fs.create_file('/side2/dir2/f2.txt')
    f2_2.st_ino = 1067962
    f2_2.st_mtime = 1600000400
    f3_2 = fs.create_file('/side2/f3.txt')
    f3_2.st_ino = 1067963
    f3_2.st_mtime = 1600000050
    f4_2 = fs.create_file('/side2/new.txt')
    f4_2.st_ino = 1060000
    f4_2.st_mtime = 1600000600
    f6_2 = fs.create_file('/side2/dir1/moved.txt')
    f6_2.st_ino = 1317976
    f6_2.st_ctime = 1600000600  # on HOME, moved to root (/side1)
    f6_2.st_mtime = 1600000070  # but newer version on NOMAD
    f7_2 = fs.create_file('/side2/dir2/RENAMED.txt')
    f7_2.st_ino = 1318067
    f7_2.st_ctime = 1600000800
    f7_2.st_mtime = 1600000050  # newer version on HOME

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_files(b)

        assert Node(f6_1.path).st_mtime == 1600000070
        assert Node(f6_2.path).st_mtime == 1600000070

        captured = capsys.readouterr()
        assert captured.out == \
            'SYNCING FILES\n'\
            'HOME: DELETE deleted.txt\n'\
            'HOME: RENAME dir2/renamed.txt AS dir2/RENAMED.txt\n'\
            'NOMAD: MOVE dir1/moved.txt TO moved.txt\n'\
            'HOME: NEW new.txt\n'\
            'NOMAD: UPDATE dir1/f1.txt\n'\
            'NOMAD: UPDATE dir2/RENAMED.txt\n'\
            'HOME: UPDATE dir2/f2.txt\n'\
            'HOME: UPDATE moved.txt\n'


def test_sync_files_no_change(fs, capsys, mocker):
    content = """[synced.side1]
name = 'HOME'
path = '/side1/'

[synced.side2]
name = 'NOMAD'
path = '/side2/'
"""
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/config.toml', contents=content)
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/inodes.sqlite3')
    fs.create_file(ZANO_LOCAL_SHARE / 'BUNDLE/ts.toml')

    fs.create_dir('/side1/')
    fs.create_dir('/side2/')

    mocker.patch('zano.core.bundle.Bundle.get_timestamp',
                 return_value=1600000200)

    d1_1 = fs.create_dir('/side1/dir1/')
    d1_1.st_ino = 1316923
    d2_1 = fs.create_dir('/side1/dir2/')
    d2_1.st_ino = 1316924
    f1_1 = fs.create_file('/side1/dir1/f1.txt')
    f1_1.st_ino = 1067612
    f1_1.st_mtime = 1600000100
    f2_1 = fs.create_file('/side1/dir2/f2.txt')
    f2_1.st_ino = 1067502
    f2_1.st_mtime = 1600000100
    f3_1 = fs.create_file('/side1/f3.txt')
    f3_1.st_ino = 1067277
    f3_1.st_mtime = 1600000050

    d1_2 = fs.create_dir('/side2/dir1/')
    d1_2.st_ino = 1317712
    d2_2 = fs.create_dir('/side2/dir2/')
    d2_2.st_ino = 1317969
    f1_2 = fs.create_file('/side2/dir1/f1.txt')
    f1_2.st_ino = 1065664
    f1_2.st_mtime = 1600000100
    f2_2 = fs.create_file('/side2/dir2/f2.txt')
    f2_2.st_ino = 1067962
    f2_2.st_mtime = 1600000100
    f3_2 = fs.create_file('/side2/f3.txt')
    f3_2.st_ino = 1067963
    f3_2.st_mtime = 1600000050

    b = Bundle('BUNDLE')

    with database.ContextManager(TESTDB_PATH, testing=True) as cursor:
        b.set_idb(cursor)
        sync_files(b)

        captured = capsys.readouterr()
        assert captured.out ==  \
            'SYNCING FILES\n'\
            'No change\n'
