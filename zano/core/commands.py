# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys
from itertools import zip_longest

from microlib import database
from microlib import terminal

from zano.core.env import _gb, _ob
from zano.core.bundle import Bundle
from zano.core.errors import CommandError
from zano.core.fs_tools import tree_structures_are_synced
from zano.core.fs_tools import list_tree_structure
from zano.core.fs_tools import updateable_files
from zano.core.tasks import backup
from zano.core.tasks import retrieve_new_files_from_connected_replicas
from zano.core.tasks import push_changes_to_replicas
from zano.core.tasks import sync_tree_structures
from zano.core.tasks import sync_files
from zano.core.tasks import check_connection


def diff(bundle_name):
    b = Bundle(bundle_name)

    # check both synced sides are connected
    check_connection(b)

    s1 = b.synced['side1']['path']
    s2 = b.synced['side2']['path']
    name1 = b.synced['side1']['name']
    name2 = b.synced['side2']['name']
    list1 = list_tree_structure(s1.path, filters=b.filters, name=name1,
                                progressbar=True)
    list2 = list_tree_structure(s2.path, filters=b.filters, name=name2,
                                progressbar=True)
    if tree_structures_are_synced(s1.path, s2.path, filters=b.filters,
                                  progressbar=True, name1=name1, name2=name2,
                                  list1=list1, list2=list2):
        print(_gb(f'No missing nodes, {name1} and {name2} are synchronized.'))
        if updateable_files(s1.path, s2.path, list1, list2, progressbar=True):
            print(_ob('Yet some files need to be updated.'))
        else:
            print(_gb('And all files are up-to-date.'))
    else:
        sl1 = list(list1)
        sl2 = list(list2)
        msg = 'Building missing nodes lists...'
        sys.stdout.write(msg)
        sys.stdout.flush()
        list1 = [name1] + [node for node in sl1 if node not in sl2]
        list2 = [name2] + [node for node in sl2 if node not in sl1]
        rows = list(zip_longest(list1, list2, fillvalue=''))
        sys.stdout.write('\r' + ' ' * len(msg) + '\r')
        sys.stdout.flush()
        print(_ob('Missing nodes lists:'))
        print(terminal.tabulate(rows))


def pair(bundle_name):
    """Build the inodes' db of two synced tree structures."""
    b = Bundle(bundle_name)

    # check both synced sides are connected
    check_connection(b)

    # check the idb does not exist
    if b.paths['db'].is_file():
        raise CommandError(f'Bundle {bundle_name} is already paired. Please '
                           f'either remove (or backup) the database yourself '
                           f'or use the sync command instead. Database is '
                           f"located at {b.paths['db']}")

    # collect dirs and files missing on each side...
    s1 = b.synced['side1']['path']
    s2 = b.synced['side2']['path']
    name1 = b.synced['side1']['name']
    name2 = b.synced['side2']['name']
    if not tree_structures_are_synced(s1.path, s2.path, filters=b.filters,
                                      progressbar=True, name1=name1,
                                      name2=name2):
        raise CommandError('The pair command can be used to build the '
                           'inodes database between two tree structures that '
                           'are already synced. Please ensure the files are '
                           'the same on both sides before pairing.')

    # when both sides are synced, collect the inodes pairs and fill the idb
    b.create_idb()

    with database.ContextManager(b.paths['db']) as cursor:
        b.set_idb(cursor)
        b.build_idb()

    # set the timestamps
    b.create_ts_file()
    b.set_timestamp()
    for r in b.replicas:
        b.set_timestamp(r)


def sync(bundle_name, backup_before=False, backup_after=False):
    # check the idb DOES exist
    b = Bundle(bundle_name)
    if not b.paths['db'].is_file():
        raise CommandError(f'Bundle {bundle_name} is not paired yet. '
                           f'Please use the pair command before syncing.')

    # check both synced sides are connected
    check_connection(b)

    # run backup before
    if backup_before:
        backup(b, 'before')

    retrieve_new_files_from_connected_replicas(b)

    with database.ContextManager(b.paths['db']) as cursor:
        b.set_idb(cursor)

        # SYNC tree structures
        sync_tree_structures(b)
        b.rebuild_idb(['dirs'])

        # SYNC FILES
        sync_files(b)
        b.rebuild_idb(['files'])

        # PUSH the changes to the connected replicas
        # (and update replicas' timestamps)
        push_changes_to_replicas(b)

    # run backup after
    if backup_after and terminal.ask_yes_no('Backup changes?'):
        backup(b, 'after')
