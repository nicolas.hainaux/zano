# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import os
import errno
from time import time
from copy import deepcopy

import toml
from microlib import database, terminal

from .env import ZANO_LOCAL_SHARE
from .errors import ConfigFileError
from .errors import NoSuchBundleError
from .fs_nodes import Node
from .fs_tools import collect_files_inodes, collect_dirs_inodes
from .fs_tools import collect_paired_dirs_inodes
from .fs_tools import collect_paired_files_inodes


def _check_config(config, path):
    keys = set(config)
    if 'synced' not in keys:
        raise ConfigFileError(f'Missing [synced] table in config file located '
                              f'at: {path}')
    keys.remove('synced')
    if 'replicas' in keys:
        keys.remove('replicas')
    if keys:
        raise ConfigFileError(f'Extraneous entries: '
                              f'{", ".join([k for k in keys])}, '
                              f'in config file located at: {path}')
    cfg = deepcopy(config)
    cfg['synced'].pop('filters', None)
    if len(cfg['synced']) != 2:
        raise ConfigFileError(f'The [synced] table must define exactly two '
                              f'sides in config file located at: {path}')
    for side in ['side1', 'side2']:
        if side not in cfg['synced']:
            raise ConfigFileError(f'Missing "{side}" in [synced] table in '
                                  f'config file located at: {path}')
        for entry in ['name', 'path']:
            if entry not in cfg['synced'][side]:
                raise ConfigFileError(f'Missing "{entry}" in [synced.{side}] '
                                      f'table in config file located at: '
                                      f'{path}')
        for entry in cfg['synced'][side]:
            if entry not in ['name', 'path', 'backup']:
                raise ConfigFileError(f'Unknown "{entry}" in [synced.{side}] '
                                      f'table in config file located at: '
                                      f'{path}')
    if 'replicas' not in cfg:
        cfg['replicas'] = []
    for r in cfg['replicas']:
        if 'path' not in cfg['replicas'][r].keys():
            raise ConfigFileError(f'Missing \'path\' in replica "{r}" '
                                  f'in config file located at: {path}')


class Bundle(object):

    def __init__(self, name):
        """Record bundle's synced and replicas."""
        path = Node(ZANO_LOCAL_SHARE) / name
        cfg_path = path / 'config.toml'
        try:
            with open(cfg_path.path) as f:
                config = toml.load(f)
        except FileNotFoundError:
            raise NoSuchBundleError(f'Found no bundle named {name} configured '
                                    f'in {ZANO_LOCAL_SHARE}.')
        self._idb = None
        _check_config(config, cfg_path)
        synced = config['synced']
        self._filters = config['synced'].pop('filters', [])
        self._synced = {}
        for s in synced:
            backup = synced[s].get('backup', None)
            if backup is not None:
                backup = Node(str(backup))
            self._synced[s] = {'name': str(synced[s]['name']),
                               'path': Node(str(synced[s]['path'])),
                               'backup': backup}
        self._replicas = {}
        if 'replicas' not in config:
            config['replicas'] = []
        for r in config['replicas']:
            rdata = config['replicas'][r]
            self._replicas[r] = {'path': Node(str(rdata['path'])),
                                 'filters': rdata.get('filters', [])}
        self._paths = {'config': Node(cfg_path),
                       'ts': path / 'ts.toml',
                       'db': path / 'inodes.sqlite3',
                       'dir': path}

    @property
    def synced(self):
        return self._synced

    @property
    def filters(self):
        return self._filters

    @property
    def replicas(self):
        return self._replicas

    @property
    def paths(self):
        return self._paths

    def set_idb(self, cursor):
        """Record the db cursor."""
        self._idb = database.Operator(cursor)

    @property
    def idb(self):
        return self._idb

    def create_idb(self):
        """Create the bundle's inodes database."""
        if self.paths['db'].is_file():
            raise FileExistsError(errno.EEXIST, os.strerror(errno.EEXIST),
                                  str(self.paths['db']))
        os.makedirs(self.paths['dir'].path, mode=0o770, exist_ok=True)
        self.paths['db'].touch(exist_ok=True)
        with database.ContextManager(self.paths['db'].path) as cursor:
            db = database.Operator(cursor)
            db.create_table('dirs', ['inode1', 'inode2'])
            db.create_table('files', ['inode1', 'inode2'])

    def build_idb(self, tables=None):
        """Fill the bundle's inodes database tables."""
        if tables is None:
            tables = ['dirs', 'files']
        root1 = self.synced['side1']['path']
        root2 = self.synced['side2']['path']
        collect_paired_inodes = {'dirs': collect_paired_dirs_inodes,
                                 'files': collect_paired_files_inodes}
        for t in tables:
            pairs = collect_paired_inodes[t](root1, root2,
                                             filters=self.filters)
            self.insert_inodes(t, pairs)

    def rebuild_idb(self, tables=None):
        """Delete and refill the bundle's inodes database tables."""
        if tables is None:
            tables = ['dirs', 'files']
        for t in tables:
            self._idb.drop_table(t)
            self._idb.create_table(t, ['inode1', 'inode2'])
        self.build_idb(tables)

    def insert_inodes(self, files_or_dirs, rows):
        """Insert provided rows to either 'files' or 'dirs' table."""
        self._idb.insert_rows(files_or_dirs, rows)

    def remove_inodes(self, files_or_dirs, inode1_or_2, inodes_values):
        """Remove the rows matching the provided inodes."""
        inodes = f"({','.join([str(_) for _ in inodes_values])})"
        where_clause = f'{inode1_or_2} IN {inodes}'
        matching = self._idb.get_rows(['id'], files_or_dirs, where_clause)
        # unpack the [(1, ), (4, ), ...] to [1, 4, ...]:
        matching = [_[0] for _ in matching]
        self._idb.remove_rows(files_or_dirs, matching)

    def is_known_inode(self, files_or_dirs, inode1_or_2, value):
        """Tell whether the provided inode is in the idb."""
        rows = self._idb.get_rows([inode1_or_2], files_or_dirs,
                                  f'{inode1_or_2} = {value}')
        return len(rows) != 0

    def get_inode_matching(self, files_or_dirs, inode1_or_2, value):
        """Get inode matching the inode value provided as inode1_or_2."""
        provided = inode1_or_2
        looked_for = {'inode1': 'inode2',
                      'inode2': 'inode1'}[provided]
        where_clause = f'{provided} = {value}'
        result = self._idb.get_rows([looked_for], files_or_dirs, where_clause)
        try:
            return int(result[0][0])
        except IndexError:  # No result
            return None

    def create_ts_file(self):
        replicas = '\n'.join([f'{r} = 0' for r in self.replicas])
        if replicas:
            replicas = '\n' + replicas
        default_content = f"""synced = 0{replicas}
"""
        with open(self.paths['ts'].path, 'w') as f:
            f.write(default_content)

    def get_timestamp(self, replica=None):
        """Get synced's timestamp or for the provided replica."""
        entry = 'synced'
        if replica is not None:
            entry = replica
        with open(self.paths['ts'].path) as f:
            ts = toml.load(f)
        return ts[entry]

    def set_timestamp(self, replica=None):
        """Update synced's timestamp or for the provided replica."""
        entry = 'synced'
        if replica is not None:
            entry = replica
        with open(self.paths['ts'].path) as f:
            ts = toml.load(f)
        ts[entry] = time()
        with open(self.paths['ts'].path, 'w') as f:
            toml.dump(ts, f)

    def connected_replicas(self):
        connected_replicas = dict()
        for r in self.replicas:
            if self.replicas[r]['path'].is_dir():
                connected_replicas[r] = self.replicas[r]
            else:
                terminal.echo_warning(f'Cannot find replica {r} '
                                      f"(path: {self.replicas[r]['path']})")
        return connected_replicas

    def fetch_new_dirs(self):
        result = {'side1': [], 'side2': []}
        for di, dp in collect_dirs_inodes(self.synced['side1']['path'],
                                          filters=self.filters):
            if not self.is_known_inode('dirs', 'inode1', di):
                result['side1'].append((di, dp))
        for di, dp in collect_dirs_inodes(self.synced['side2']['path'],
                                          filters=self.filters):
            if not self.is_known_inode('dirs', 'inode2', di):
                result['side2'].append((di, dp))
        return result

    def fetch_new_files(self):
        result = {'side1': [], 'side2': []}
        for di, dp in collect_files_inodes(self.synced['side1']['path'],
                                           filters=self.filters):
            if not self.is_known_inode('files', 'inode1', di):
                result['side1'].append((di, dp))
        for di, dp in collect_files_inodes(self.synced['side2']['path'],
                                           filters=self.filters):
            if not self.is_known_inode('files', 'inode2', di):
                result['side2'].append((di, dp))
        return result
