# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import shutil

import blessed
from microlib import terminal
from send2trash import send2trash

from zano.core.errors import MissingPathError
from zano.core.env import _gb, _yb, _ob, _rb, _bb
from zano.core.fs_nodes import Node
from zano.core.fs_tools import collect_newer_files
from zano.core.fs_tools import skim_missing_dirs
from zano.core.fs_tools import collect_missing_files
from zano.core.fs_tools import tree_structures_are_synced
from zano.core.fs_tools import find_dir_matching_inode
from zano.core.fs_tools import find_file_matching_inode
from zano.core.fs_tools import moved_node
from zano.core.fs_tools import renamed_node
from zano.core.fs_tools import is_accepted
from zano.core.fs_tools import conflict
from zano.core.fs_tools import most_recent_modified
from zano.core.fs_tools import _browse_files
from zano.core.fs_tools import rsync

PERMISSION_ERRORS_INFO = '\nPlease fix the permission issue and run '\
    'zano again.'


def check_connection(b):
    err_msg = []
    for n in ['1', '2']:
        if not b.synced[f'side{n}']['path'].is_dir():
            err_msg.append(f"{b.synced[f'side{n}']['name']} cannot be found "
                           f"(missing path: {b.synced[f'side{n}']['path']})")
    if err_msg:
        raise MissingPathError('\n'.join(err_msg))


def retrieve_new_files_from_connected_replicas(b):
    connected_replicas = b.connected_replicas()
    for r in connected_replicas:
        found_new_files = False
        replica_root = connected_replicas[r]['path']
        for f in collect_newer_files(replica_root, ts=b.get_timestamp(r),
                                     filters=b.filters):
            source = replica_root / f
            dest1 = b.synced['side1']['path'] / f
            dest2 = b.synced['side2']['path'] / f
            if not found_new_files:
                term = blessed.Terminal()
                print(term.green(term.bold(f'NEW FILES FOUND ON {r}')))
                found_new_files = True
            print(f'{f}')
            if not dest1.parent.is_dir():
                dest1.mkdir(parents=True, exist_ok=True)
            if not dest2.parent.is_dir():
                dest2.mkdir(parents=True, exist_ok=True)
            s = Node(source, mask=replica_root)
            try:
                shutil.copy2(source.path, dest1.path)
            except PermissionError:
                n = b.synced['side1']['name']
                terminal.echo_error(f'insufficient permissions to copy '
                                    f'{s.name} to {n} ({dest1.parent})'
                                    + PERMISSION_ERRORS_INFO)
            try:
                shutil.copy2(source.path, dest2.path)
            except PermissionError:
                n = b.synced['side2']['name']
                terminal.echo_error(f'insufficient permissions to copy '
                                    f'{s.name} to {n} ({dest2.parent})'
                                    + PERMISSION_ERRORS_INFO)


def backup(b, when):
    """
    rsync data to both synced backups. Assumes the backup paths are defined.
    """
    print(_bb('BACKING UP EACH SYNCED SIDE'))
    err_msg = []
    for s in ['side1', 'side2']:
        if b.synced[s]['backup'] is None:
            err_msg.append(f"--backup-{when} option requires "
                           f"[synced.{s}] to define a backup path "
                           "(backup = /path/to/backup)")
        elif not b.synced[s]['backup'].is_dir():
            n = b.synced[s]['name']
            bp = b.synced[s]['backup']
            err_msg.append(f"Cannot find backup directory for {n}: {bp}")
    if err_msg:
        raise MissingPathError('\n'.join(err_msg))
    for s in ['side1', 'side2']:
        source = b.synced[s]['path']
        dest = b.synced[s]['backup']
        options = '-avzz --progress --delete --delete-excluded '\
            '--delete-before --ignore-errors --force'
        rsync(source, dest, options)
    print('Done')


def push_changes_to_replicas(b):
    connected_replicas = b.connected_replicas()
    for r in connected_replicas:
        do_push = terminal.ask_yes_no(f'Push changes to {r}?')
        if do_push:
            filters = connected_replicas[r]['filters']
            source = b.synced['side1']['path']
            dest = connected_replicas[r]['path']
            options = '-rtv --delete --delete-excluded --force '\
                '--modify-window=2'
            rsync(source, dest, options, filters)
            b.set_timestamp(replica=r)


def _rescue_nodes(b, reldir, root_node, one, two):
    """
    Before deleting directory (reldir) inside root_node, check if some nodes
    have not been moved to another place outside root_node and moved them too.
    """
    s1 = Node(b.synced[f'side{one}']['path'])
    s2 = Node(b.synced[f'side{two}']['path'])
    name1 = b.synced[f'side{one}']['name']
    D = Node(s1 / reldir, mask=s1)
    for path in sorted(D.iterdir()):
        N1 = Node(path, mask=s1)
        if is_accepted(N1, b.filters):
            if N1.is_file():
                find_node_matching_inode = find_file_matching_inode
                files_or_dirs = 'files'
            elif N1.is_dir():
                find_node_matching_inode = find_dir_matching_inode
                files_or_dirs = 'dirs'
            nodetype = {'files': 'file', 'dirs': 'directory'}[files_or_dirs]
            inode1 = N1.st_ino
            inode2 = b.get_inode_matching(files_or_dirs, f'inode{one}', inode1)
            if inode2 is None:
                print(_rb(f'{name1}: CAUTION: found NEW {nodetype} '
                          f'{N1.relpath} in to-be-deleted directory {reldir}; '
                          'you might want to get it back from the '
                          'trashcan...'))
            else:
                filters = b.filters + [f'{root_node}/*']
                n2 = find_node_matching_inode(s2, inode2, filters=filters)
                if n2 is not None:
                    dest1 = Node(s1 / n2, mask=s1)
                    msg = _yb(f'{name1}: MOVE ') + f'{N1.relpath} ' \
                        + _yb('TO') + f' {dest1.relpath}'
                    print(msg)
                    try:
                        # DO NOT copy mtime since it could hide changes on one
                        # side. Moving a file changes its ctime, not mtime.
                        shutil.move(N1.path, dest1.path)
                    except PermissionError:
                        terminal.echo_error(f'insufficient permissions to '
                                            f'move {N1.relpath} to '
                                            f'{dest1.relpath} on {name1}'
                                            + PERMISSION_ERRORS_INFO)
        if N1.is_dir():
            _rescue_nodes(b, N1.relpath, root_node, one, two)


def _handle_missing_node(b, files_or_dirs, one, two, n1):
    s1 = Node(b.synced[f'side{one}']['path'])
    s2 = Node(b.synced[f'side{two}']['path'])
    name1 = b.synced[f'side{one}']['name']
    name2 = b.synced[f'side{two}']['name']
    find_node_matching_inode = \
        {'files': find_file_matching_inode,
         'dirs': find_dir_matching_inode}[files_or_dirs]
    N1 = Node(s1 / n1, mask=s1)
    inode1 = N1.st_ino
    if b.is_known_inode(files_or_dirs, f'inode{one}', inode1):
        # n1 is not new on side1
        inode2 = b.get_inode_matching(files_or_dirs, f'inode{one}', inode1)
        n2 = find_node_matching_inode(s2, inode2, filters=b.filters)
        if n2 is None:
            # n1 is not new, yet no matching n2 can be found on side2:
            # n2 has been deleted from side2, and so should n1 be
            # so let's move n1 to user's trash
            # But before that, let's rescue possible nodes located outside
            # of n1:
            if N1.is_dir():
                _rescue_nodes(b, n1, n1, one, two)
            print(_ob(f'{name1}: DELETE {n1.relpath}'))
            try:
                send2trash(N1.path)
            except PermissionError:
                terminal.echo_error(f'insufficient permissions to move '
                                    f'{N1.path} to trash'
                                    + PERMISSION_ERRORS_INFO)
            else:
                # if n1 is a directory, it should not be necessary to remove
                # inodes from its subfolders
                b.remove_inodes(files_or_dirs, f'inode{one}', [inode1])
        else:
            # n1 is not new, but a matching node has been found:
            # one of them has been moved or renamed, or both;
            # inodes can be kept as they are
            N2 = Node(s2 / n2, mask=s2)
            renamed = renamed_node(N1, N2)
            moved = moved_node(N1, N2)
            if renamed:
                action = 'RENAME'
                lword = 'AS'
                changed = renamed
            elif moved:
                action = 'MOVE'
                lword = 'TO'
                changed = moved
            else:
                terminal.echo_error((f'found matching nodes: {N1} and {N2}, '
                                     'but impossible to find out which one is '
                                     'the moved or renamed version of the '
                                     'other. Please perform the correction '
                                     'and run zano again.'))
            if changed == 1:
                nodes = [s2 / n2, s2 / n1]
                relpaths = [n2, n1]
                msg = _yb(f'{name2}: {action} ') + f'{n2} ' \
                    + _yb(f'{lword}') + f' {n1}'
            elif changed == 2:
                nodes = [s1 / n1, s1 / n2]
                relpaths = [n1, n2]
                msg = _yb(f'{name1}: {action} ') + f'{n1} ' \
                    + _yb(f'{lword}') + f' {n2}'
            print(msg)
            try:
                # DO NOT copy mtime since it could hide changes on one side
                # Moving a file changes its ctime, not mtime.
                shutil.move(nodes[0].path, nodes[1].path)
            except PermissionError:
                name = {1: name2, 2: name1}[changed]
                terminal.echo_error(f'insufficient permissions to '
                                    f'{action.lower()} {relpaths[0]} '
                                    f'{lword.lower()} {relpaths[1]} on '
                                    f'{name}' + PERMISSION_ERRORS_INFO)

    else:  # new node on side1 that does not yet exist on side2
        print(_gb(f'{name2}: NEW ') + f'{n1}')
        path2 = s2 / n1
        try:
            if files_or_dirs == 'dirs':
                path2.mkdir()
                path2.set_mtime(N1.st_mtime)
            else:
                shutil.copy2(N1.path, path2.path)
        except PermissionError:
            terminal.echo_error(f'insufficient permissions to create '
                                f'{s2 / n1}'
                                + PERMISSION_ERRORS_INFO)
        else:
            inode2 = path2.st_ino
            inodes = (inode1, inode2) if one == '1' else (inode2, inode1)
            b.insert_inodes(files_or_dirs, [inodes])


def sync_tree_structures(b):
    """
    Synchronize tree structures of both bundle's sides.
    The connection to Bundle b's idb is supposed to be available.
    """
    print(_bb('SYNCING DIRECTORIES'))
    s1 = b.synced['side1']['path']
    s2 = b.synced['side2']['path']
    change = False
    while not tree_structures_are_synced(s1.path, s2.path, filters=b.filters,
                                         dirs_only=True):
        change = True
        for d1 in skim_missing_dirs(s1, s2, filters=b.filters):
            _handle_missing_node(b, 'dirs', '1', '2', d1)
        for d2 in skim_missing_dirs(s2, s1, filters=b.filters):
            _handle_missing_node(b, 'dirs', '2', '1', d2)
    if not change:
        print('No change')


def update_paired_files(b):
    """
    Browse (already synchronized) tree structures and update each file that
    needs to. Handle possible conflicts.
    """

    root1 = Node(b.synced['side1']['path'])
    root2 = Node(b.synced['side2']['path'])
    name1 = b.synced['side1']['name']
    name2 = b.synced['side2']['name']

    def update(f, root1, root2):
        f1 = Node(f, mask=root1)
        f2 = Node(root2 / f1.relpath, mask=root2)
        choice = most_recent_modified(f1, f2)

        if conflict(f1, f2, b.get_timestamp()):
            most_recent = ''
            if choice in ['1', '2']:
                name = b.synced[f'side{choice}']['name']
                most_recent = f' (most recently modified on {name})'

            def check_answer(s):
                return s in {name1, name2}

            choice = terminal.ask_user(
                f'{f1.relpath} has been modified on both sides{most_recent}. '
                f'Do you want to keep the version of {name1} or {name2}?',
                allowed=check_answer)
            choice = {name1: '1', name2: '2'}[choice]

        if choice == '1':
            print(_yb(f'{name2}: UPDATE ') + f'{f1.relpath}')
            shutil.copy2(f1.path, f2.path)
        elif choice == '2':
            print(_yb(f'{name1}: UPDATE ') + f'{f1.relpath}')
            shutil.copy2(f2.path, f1.path)
        if choice == '0':  # no update
            return None
        else:
            return f1.relpath

    u = _browse_files(Node(root1), update, filters=b.filters,
                      root1=root1, root2=root2)
    if u:
        return True
    return False


def sync_files(b):
    """
    Synchronize files of already synchronized tree structures.
    The connection to Bundle b's idb is supposed to be available.
    """
    print(_bb('SYNCING FILES'))
    s1 = b.synced['side1']['path']
    s2 = b.synced['side2']['path']
    synced = False
    done = False
    while not done:
        missing1 = collect_missing_files(s1, s2, filters=b.filters)
        missing2 = collect_missing_files(s2, s1, filters=b.filters)
        if not missing1 and not missing2:
            done = True
        else:
            synced = True
            for f1 in missing1:
                _handle_missing_node(b, 'files', '1', '2', f1)
            # Maybe there have been some changes, so possibly missing files
            # on side1 need to be collected again.
            for f2 in collect_missing_files(s2, s1, filters=b.filters):
                _handle_missing_node(b, 'files', '2', '1', f2)
    # UPDATE files that need to
    updated = update_paired_files(b)
    change = synced or updated
    if not change:
        print('No change')
