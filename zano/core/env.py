# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys
from pathlib import Path

import toml
import blessed

with open(Path(__file__).parent.parent / 'meta/pyproject.toml', 'r') as f:
    pp = toml.load(f)

__version__ = pp['tool']['poetry']['version']

PROG_NAME = 'Zano'
PYVER = sys.version.replace('\n', ' ')
MESSAGE = f'This is {PROG_NAME}, version {__version__}, '\
    f'running under python {PYVER}.\nCopyright (C) 2021 Nicolas Hainaux\n'\
    'This program comes with ABSOLUTELY NO WARRANTY.\n'\
    'This is free software, and you are welcome to redistribute it '\
    'under certain conditions. Its license is the GPL version 3 or later.'

ZANO_LOCAL_SHARE = Path.home() / '.local/share/zano'


def _gb(arg):
    term = blessed.Terminal()
    return term.green(term.bold(arg))


def _yb(arg):
    term = blessed.Terminal()
    return term.gold(term.bold(arg))


def _ob(arg):
    term = blessed.Terminal()
    return term.darkorange(term.bold(arg))


def _rb(arg):
    term = blessed.Terminal()
    return term.firebrick(term.bold(arg))


def _bb(arg):
    term = blessed.Terminal()
    return term.steelblue1(term.bold(arg))
