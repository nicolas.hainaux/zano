# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import os
import subprocess

from tqdm import tqdm

from zano.core import default
from zano.core.env import _rb
from zano.core.fs_nodes import Node


def _check_consistency(n1, n2):
    n1_type = {n1.is_file(): 'file', n1.is_dir(): 'directory'}[True]
    n2_type = {n2.is_file(): 'file', n2.is_dir(): 'directory'}[True]
    if n1_type == n2_type:
        return True
    raise TypeError(f'Arguments should both be a file or both a directory; got'
                    f' a {n1_type} and a {n2_type} instead.')


def is_accepted(n, filters):
    """Return True if Node n does not match any of the filters."""
    filters = list(filters)
    filters += default.FILTERS
    if n.is_dir():
        if '.git' in [x.name for x in os.scandir(str(n)) if x.is_dir()]:
            return False
    elif not n.is_file():  # n is neither a file or a dir
        print(_rb(f'IGNORING {n.path} (neither a directory nor a file).'))
        return False
    return not any(n.match(f) for f in filters)


def most_recent_modified(n1, n2):
    """Return '0' is mtimes of n1 are equals, or '1' or '2' """
    choice = '0'  # same mtime
    if n1.st_mtime > n2.st_mtime:
        choice = '1'
    elif n2.st_mtime > n1.st_mtime:
        choice = '2'
    return choice


def conflict(n1, n2, ts):
    """
    True if both nodes have been modified after provided timestamp.

    But if both mtimes are equal, we ignore the conflict: nodes may have been
    updated (or be both new) and it is very unlikely that two different
    modifications on two sides happened at the very same time.
    """
    if n1.st_mtime == n2.st_mtime:
        return False
    return n1.st_mtime > ts and n2.st_mtime > ts


def renamed_node(n1, n2):
    """
    Tell which node, among n1 and n2, has been renamed.

    This function is intended to be used when the inodes' pairs are known.

    Return values:
    0 means n1 and n2 are not a renamed version of the other
    1 means n1 is a renamed version of n2
    2 means n2 is a renamed version of n1
    """
    _check_consistency(n1, n2)
    renamed = (n1.name != n2.name and n1.relparent == n2.relparent)
    if renamed:
        return 1 if n1.st_ctime > n2.st_ctime else 2
    else:
        return 0


def moved_node(n1, n2):
    """
    Tell which node, among n1 and n2, has been moved.

    This function is intended to be used when the inodes' pairs are known.

    Return values:
    0 means n1 and n2 are not a moved version of the other (same parent)
    1 means n1 is a moved version of n2
    2 means n2 is a moved version of n1
    """
    _check_consistency(n1, n2)
    moved = (n1.relparent != n2.relparent)
    if moved:
        return 1 if n1.st_ctime > n2.st_ctime else 2
    else:
        return 0


def probably_moved(n1, n2):
    """
    Tell whether n1 and n2 are probably the same node, but moved.

    This function is intended to be used when the inodes' pairs are NOT known,
    and only tries to make a guess.

    Return values:
    0 means n1 and n2 are probably not the same node
    1 means n1 is probably a moved version of n2
    2 means n2 is probably a moved version of n1
    """
    _check_consistency(n1, n2)
    moved = n1.name == n2.name and n1.relparent != n2.relparent \
        and n1.st_mtime == n2.st_mtime
    if n1.is_file() and n2.is_file():
        moved = moved and (n1.size == n2.size)
    if moved:
        return 1 if n1.st_ctime > n2.st_ctime else 2
    return 0


def probably_renamed(n1, n2):
    """
    Tell whether n1 and n2 are probably the same node, not moved but renamed.

    This function is intended to be used when the inodes' pairs are NOT known,
    and only tries to make a guess.

    Return values:
    0 means n1 and n2 are probably not the same node
    1 means n1 is probably a renamed version of n2
    2 means n2 is probably a renamed version of n1
    """
    _check_consistency(n1, n2)
    renamed = (n1.name != n2.name and n1.relparent == n2.relparent
               and n1.st_mtime == n2.st_mtime)
    if n1.is_file() and n2.is_file():
        renamed = renamed and (n1.size == n2.size)
    if renamed:
        return 1 if n1.st_ctime > n2.st_ctime else 2
    return 0


def probably_moved_and_renamed(n1, n2):
    """
    Tell whether n1 and n2 are probably the same node, but moved and renamed.

    This function is intended to be used when the inodes' pairs are NOT known,
    and only tries to make a guess.

    Return values:
    0 means n1 and n2 are probably not the same node
    1 means n1 is probably a moved and renamed version of n2
    2 means n2 is probably a moved and renamed version of n1
    """
    _check_consistency(n1, n2)
    moved_and_renamed = (n1.name != n2.name and n1.relparent != n2.relparent
                         and n1.st_mtime == n2.st_mtime)
    if n1.is_file() and n2.is_file():
        moved_and_renamed == moved_and_renamed and (n1.size == n2.size)
    if moved_and_renamed:
        return 1 if n1.st_ctime > n2.st_ctime else 2
    return 0


def _collect_dirs_info(path, match, filters=None, **match_args):
    """
    Collect data from dirs matching the condition defined by match function.
    """
    if filters is None:
        filters = []

    collected = []
    for node in sorted(path.iterdir()):
        if node.is_dir() and is_accepted(node, filters):
            info = match(node, **match_args)
            if info is not None:
                collected.append(info)
            collected += _collect_dirs_info(node, match, filters=filters,
                                            **match_args)
    return collected


def collect_dirs_inodes(root1, filters=None):
    """
    Return the list of dirs' inodes.
    """
    if filters is None:
        filters = []

    def st_ino(d, root1):
        d = Node(d, mask=root1)
        return (d.st_ino, d.relpath)

    return _collect_dirs_info(Node(root1), st_ino, filters=filters,
                              root1=root1)


def _find_dir_matching(path, match, filters=None, **match_args):
    if filters is None:
        filters = []

    for node in sorted(path.iterdir()):
        if node.is_dir() and is_accepted(node, filters):
            info = match(node, **match_args)
            if info is not None:
                return info
            else:
                further = _find_dir_matching(node, match, **match_args,
                                             filters=filters)
                if further is not None:
                    return further
    return None


def find_dir_matching_inode(path, st_ino, filters=None):
    if filters is None:
        filters = []

    def match(d, i, root):
        if Node(d).st_ino == i:
            return Node(d, mask=root).relpath
        return None

    return _find_dir_matching(Node(path), match, filters=filters, i=st_ino,
                              root=path)


def collect_paired_dirs_inodes(root1, root2, filters=None):
    """
    Return the list of inodes' pairs.
    """
    if filters is None:
        filters = []

    def st_ino_pair(d, root1, root2):
        d1 = Node(d, mask=root1)
        path2 = Node(root2) / d1.relpath
        if path2.is_dir():
            d2 = Node(path2, mask=root2)
            return (d1.st_ino, d2.st_ino)
        else:
            return None

    return _collect_dirs_info(Node(root1), st_ino_pair, filters=filters,
                              root1=root1, root2=root2)


def _browse_files(path, fct, filters=None, **fct_args):
    """
    Browse files and collect data or apply action to matching files.
    """
    if filters is None:
        filters = []

    collected = []
    for node in sorted(path.iterdir()):
        if node.is_dir() and is_accepted(node, filters):
            collected += _browse_files(node, fct, filters=filters,
                                       **fct_args)
        elif node.is_file() and is_accepted(node, filters):
            info = fct(node, **fct_args)
            if info is not None:
                collected.append(info)
    return collected


def collect_newer_files(root1, ts, filters=None):
    if filters is None:
        filters = []

    def newer(f, ts):
        if Node(f).st_mtime > ts:
            return Node(f).relpath
        else:
            return None

    return _browse_files(root1, newer, ts=ts, filters=filters)


def collect_missing_files(root1, root2, filters=None):
    """
    Return the list of root1's files relpaths missing on root2.
    """
    if filters is None:
        filters = []

    def missing(f, root1, root2):
        f = Node(f, mask=root1)
        if (Node(root2) / f.relpath).is_file():
            return None
        else:
            return f.relpath

    return _browse_files(Node(root1), missing, root1=root1, root2=root2,
                         filters=filters)


def collect_files_inodes(root1, filters=None):
    """
    Return the list of files' inodes.
    """
    if filters is None:
        filters = []

    def st_ino(f, root1):
        f = Node(f, mask=root1)
        return (f.st_ino, f.relpath)

    return _browse_files(Node(root1), st_ino, root1=root1,
                         filters=filters)


def _find_file_matching(path, match, filters=None, **match_args):
    if filters is None:
        filters = []

    for node in sorted(path.iterdir()):
        if node.is_file() and is_accepted(node, filters):
            info = match(node, **match_args)
            if info is not None:
                return info
        elif node.is_dir() and is_accepted(node, filters):
            found = _find_file_matching(node, match, filters=filters,
                                        **match_args)
            if found is not None:
                return found
    return None


def find_file_matching_inode(path, st_ino, filters=None):
    def match(f, i, root):
        if Node(f).st_ino == i:
            return Node(f, mask=root).relpath
        return None

    return _find_file_matching(Node(path), match, i=st_ino, root=path,
                               filters=filters)


def collect_paired_files_inodes(root1, root2, filters=None):
    """
    Return the list of inodes' pairs.
    """

    if filters is None:
        filters = []

    def st_ino_pair(f, root1, root2):
        f1 = Node(f, mask=root1)
        path2 = Node(root2) / f1.relpath
        if path2.is_file():
            f2 = Node(path2, mask=root2)
            return (f1.st_ino, f2.st_ino)
        else:
            return None

    return _browse_files(Node(root1), st_ino_pair, filters=filters,
                         root1=root1, root2=root2)


def _skim_dirs_info(path, match, filters=None, **match_args):
    """
    Collect data from dirs matching the condition defined by match function,
    but do not dive further into dirs that do match.
    """
    if filters is None:
        filters = []

    collected = []
    for node in sorted(path.iterdir()):
        if node.is_dir() and is_accepted(node, filters):
            info = match(node, **match_args)
            if info is None:
                collected += _skim_dirs_info(node, match, filters=filters,
                                             **match_args)
            else:
                collected.append(info)

    return collected


def skim_missing_dirs(root1, root2, filters=None):
    """
    Return the list of first found root1's files relpaths missing on root2.
    """
    if filters is None:
        filters = []

    def missing(d, root1, root2):
        d = Node(d, mask=root1)
        if (Node(root2) / d.relpath).is_dir():
            return None
        else:
            return d.relpath

    return _skim_dirs_info(Node(root1), missing, filters=filters, root1=root1,
                           root2=root2)


def list_tree_structure(rootdir, filters=None, dirs_only=False, name=None,
                        progressbar=False):
    if filters is None:
        filters = []
    L = []
    if progressbar:
        nb = len(list(os.walk(rootdir)))
        desc = ''
        if name is not None:
            desc = f'Browse tree structure of {name}'
        tree_structure = tqdm(os.walk(rootdir), total=nb, desc=desc,
                              leave=False)
    else:
        tree_structure = os.walk(rootdir)
    for root, dirs, files in tree_structure:
        if (is_accepted(Node(root), filters)
           and all(is_accepted(Node(n), filters)
                   for n in Node(root).parents)):
            if Node(root).path != Node(rootdir).path:
                L.append(str(Node(root, mask=rootdir).relpath))
            if not dirs_only:
                for f in files:
                    F = Node(root) / f
                    if is_accepted(Node(F), filters):
                        L.append(str(Node(F, mask=rootdir).relpath))
    return sorted(L)


def tree_structures_are_synced(root1, root2, filters=None, dirs_only=False,
                               progressbar=False, name1=None, name2=None,
                               list1=None, list2=None):
    """True if tree structures root1 and root2 are identical."""
    if filters is None:
        filters = []
    if list1 is None:
        list1 = list_tree_structure(root1, filters=filters,
                                    dirs_only=dirs_only, name=name1,
                                    progressbar=progressbar)
    if list2 is None:
        list2 = list_tree_structure(root2, filters=filters,
                                    dirs_only=dirs_only, name=name2,
                                    progressbar=progressbar)
    if len(list1) != len(list2):
        return False
    if progressbar:
        list1 = tqdm(list1, total=len(list1),
                     desc=f'Looking for missing nodes in {name2}', leave=False)
    for n in list1:
        if n not in list2:
            return False
    if progressbar:
        list2 = tqdm(list2, total=len(list2),
                     desc=f'Looking for missing nodes in {name1}', leave=False)
    for n in list2:
        if n not in list1:
            return False
    return True


def updateable_files(root1, root2, list1, list2, progressbar=False):
    updateable = False
    couples = zip(list1, list2)
    if progressbar:
        couples = tqdm(couples, leave=False, total=len(list1),
                       desc='Looking for updateable files')
    for f1, f2 in couples:
        n1 = Node(root1) / f1
        n2 = Node(root2) / f2
        if n1.is_file() and n1.st_mtime != n2.st_mtime:
            updateable = True
    return updateable


def rsync(source, dest, options='', filters=None):
    if filters is None:
        filters = []
    options = options.split()
    filters = [('--filter', f'- {i}') for i in filters]
    filters = [val for pair in filters for val in pair]  # flatten filters
    subprocess.run(['rsync', *options, *filters, str(source) + '/', str(dest)])
