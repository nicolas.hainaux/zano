# -*- coding: utf-8 -*-

# Zano is a bidirectional synchronization helper.
# Copyright 2021 Nicolas Hainaux <nh.techn@gmail.com>

# This file is part of Zano.

# Zano is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.

# Zano is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Zano; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import click

from microlib.terminal import echo_error

from .core import env, commands
from .core.errors import ZanoError


__all__ = ['run']
__version__ = env.__version__


@click.group()
@click.version_option(version=__version__, prog_name=env.PROG_NAME,
                      message=env.MESSAGE)
def run():
    """Help synchronize folders, even when both sides have changes."""


def _cmd(cmd, *args, do_click_echo=echo_error, **kwargs):
    """Generic command"""
    try:
        cmd(*args, **kwargs)
    except ZanoError as e:
        do_click_echo(str(e))


@run.command('sync')
@click.argument('name')
@click.option('-B', '--backup-before', is_flag=True, default=False,
              show_default=True, help='if true, a backup of each synced side '
              'will be performed BEFORE the synchronization. Requires to '
              'provide a backup path for each synced side in the bundle\'s '
              'config file. Backups are not filtered, so everything can be '
              'restored from them.')
@click.option('-b', '--backup-after', is_flag=True, default=False,
              show_default=True, help='if true, a backup of each synced side '
              'will be performed AFTER the synchronization. Requires to '
              'provide a backup path for each synced side in the bundle\'s '
              'config file. Backups are not filtered, so everything can be '
              'restored from them.')
def sync(name, backup_before, backup_after):
    """
    Synchronize two directories.
    """
    _cmd(commands.sync, name, backup_before=backup_before,
         backup_after=backup_after)


@run.command('pair')
@click.argument('name')
def pair(name):
    """
    Pair two directories.
    """
    _cmd(commands.pair, name)


@run.command('diff')
@click.argument('name')
def diff(name):
    """
    Compare the two tree structures (show missing files or directories)
    """
    _cmd(commands.diff, name)
