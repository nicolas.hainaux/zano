.. role:: toml(code)
   :language: toml

.. role:: bash(code)
   :language: bash

Overview
========

Zano is a bidirectional synchronization helper for Linux.

It may be useful if you need to synchronize two tree structures and can not or do not want to rely on an Internet connection to perform this synchronization (via a cloud storage service, for instance).

.. warning::
    Zano is not tested enough to be used in production yet. As it needs to move or rename files and directories, it may introduce some disorder in your data, so please keep a backup of them before using Zano! Note that Zano will send files or directories to be deleted to trash instead of deleting them. This should prevent any unwished deletions.

Quick start
===========

Zano needs to "pair" your two tree structures in a synchronized state. So, make sure they're identical for the first run of Zano. For instance, create one by copying the other.

Once this is done, you'll have to configure a bundle. Choose a name for it, say WORK, and define at least the two paths that you want to keep synchronized, like in the minimal example below:

.. code-block:: toml

    [synced.side1]
    name = 'HOME'
    path = '/home/johndoe/work/docs/'

    [synced.side2]
    name = 'NOMAD'
    path = '/media/johndoe/MY_NOMAD_KEY'

    [replicas]

where :toml:`[synced.side1]` and :toml:`[synced.side2]` are mandatory.

Save this file as :bash:`~/.local/share/zano/WORK/config.toml`.

Plug your nomad key, mount it and pair your tree structures: :bash:`zano pair WORK`

Now you can work at home, and/or abroad on your nomad copy. When you come back, mount your nomad key and run :bash:`zano sync WORK`

`Source code <https://gitlab.com/nicolas.hainaux/zano>`__
